//
//  SurgicalReportStep2VC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 29/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalReportStep2VC: SurgicalReportVC {

    @IBOutlet weak var textFieldConnective: MCTextField!
    @IBOutlet weak var textFieldExtraction: MCTextField!
    @IBOutlet weak var textFieldBiopsy: MCTextField!
    @IBOutlet weak var textFieldRoot: MCTextField!
    @IBOutlet weak var textFieldCrown: MCTextField!
    @IBOutlet weak var textFieldRidge: MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValues()
    }
    
    func loadValues() {
        textFieldConnective.text = surgicalReport.connectiveTissueGraft
        textFieldExtraction.text = surgicalReport.extraction
        textFieldBiopsy.text = surgicalReport.biopsy
        textFieldRoot.text = surgicalReport.rootAssumption
        textFieldCrown.text = surgicalReport.crownLengthening
        textFieldRidge.text = surgicalReport.ridgeAugmentation
    }
    
    func saveValues() {
        self.view.endEditing(true)
        
        surgicalReport.connectiveTissueGraft = textFieldConnective.text!
        surgicalReport.extraction = textFieldExtraction.text!
        surgicalReport.biopsy = textFieldBiopsy.text!
        surgicalReport.rootAssumption = textFieldRoot.text!
        surgicalReport.crownLengthening = textFieldCrown.text!
        surgicalReport.ridgeAugmentation = textFieldRidge.text!
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonNextAction () {
        saveValues()
        
        let step3VC = self.storyboard?.instantiateViewController(withIdentifier: "kSurgicalReportStep3VC") as! SurgicalReportStep3VC
        step3VC.patient = self.patient
        self.navigationController?.pushViewController(step3VC, animated: true)
    }
}
