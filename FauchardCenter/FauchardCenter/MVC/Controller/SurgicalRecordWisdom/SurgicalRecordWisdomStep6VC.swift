//
//  SurgicalRecordWistomStep6VC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 29/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalRecordWisdomStep6VC: SurgicalRecordWisdomVC {
    
    @IBOutlet weak var radioBiopsy: RadioButton!
    @IBOutlet weak var radioTissue: RadioButton!
    @IBOutlet weak var radioWalls: RadioButton!
    @IBOutlet weak var radioHealingStent: RadioButton!
    @IBOutlet weak var radioBoneGraft: RadioButton!
    
    @IBOutlet weak var viewBoneGraft: UIView!
    
    @IBOutlet weak var buttonAutogenous: UIButton!
    @IBOutlet weak var buttonAllograft: UIButton!
    @IBOutlet weak var buttonConnective: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValues()
    }
    
    func loadValues() {
        
        radioBiopsy.setSelectedWithTag(surgicalRecordWisdom.isBiopsy == nil ? 0 : surgicalRecordWisdom.isBiopsy)
        radioTissue.setSelectedWithTag(surgicalRecordWisdom.isHardTissue == nil ? 0 : surgicalRecordWisdom.isHardTissue)
        radioWalls.setSelectedWithTag(surgicalRecordWisdom.isOsseousWalls == nil ? 0 : surgicalRecordWisdom.isOsseousWalls)
        radioHealingStent.setSelectedWithTag(surgicalRecordWisdom.isHealingStentUsed == nil ? 0 : surgicalRecordWisdom.isHealingStentUsed)
        radioBoneGraft.setSelectedWithTag(surgicalRecordWisdom.isBoneCraft == nil ? 0 : surgicalRecordWisdom.isBoneCraft)
        
        buttonAllograft.isSelected = surgicalRecordWisdom.isAllograft == nil ? false : surgicalRecordWisdom.isAllograft
        buttonAutogenous.isSelected = surgicalRecordWisdom.isAutogenous == nil ? false : surgicalRecordWisdom.isAutogenous
        buttonConnective.isSelected = surgicalRecordWisdom.isConnectiveTissue == nil ? false : surgicalRecordWisdom.isConnectiveTissue
        
        radioBoneGraftAction()
    }
    
    func saveValues() {
        self.view.endEditing(true)
        
        surgicalRecordWisdom.isBiopsy = radioBiopsy.selected == nil ? 0 : radioBiopsy.selected.tag
        surgicalRecordWisdom.isHardTissue = radioTissue.selected == nil ? 0 : radioTissue.selected.tag
        surgicalRecordWisdom.isOsseousWalls = radioWalls.selected == nil ? 0 : radioWalls.selected.tag
        surgicalRecordWisdom.isHealingStentUsed = radioHealingStent.selected == nil ? 0 : radioHealingStent.selected.tag
        surgicalRecordWisdom.isBoneCraft = radioBoneGraft.selected == nil ? 0 : radioBoneGraft.selected.tag
        
        surgicalRecordWisdom.isAllograft = buttonAllograft.isSelected
        surgicalRecordWisdom.isAutogenous = buttonAutogenous.isSelected
        surgicalRecordWisdom.isConnectiveTissue = buttonConnective.isSelected
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonNextAction() {
        saveValues()
        
        let step7VC = self.storyboard?.instantiateViewController(withIdentifier: "kSurgicalRecordWisdomStep7VC") as! SurgicalRecordWisdomStep7VC
        step7VC.patient = self.patient
        self.navigationController?.pushViewController(step7VC, animated: true)
    }
    
    @IBAction func buttonBoneGraftAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.tag == 1 {
            if sender.isSelected {
                AutogenousPopup.popUpView().showInViewController(self, completion: { (popUpView, harvestedFromTextField, volumeTextField) in
                    popUpView.close()
                    self.surgicalRecordWisdom.isAutogenous = true
                    self.surgicalRecordWisdom.autogenousHarvestedFrom = harvestedFromTextField.text!
                    self.surgicalRecordWisdom.autogenousVolume = volumeTextField.text!
                })
            } else {
                self.surgicalRecordWisdom.isAutogenous = false
                self.surgicalRecordWisdom.autogenousHarvestedFrom = ""
                self.surgicalRecordWisdom.autogenousVolume = ""
            }
        } else if sender.tag == 2 {
            if sender.isSelected {
                AllograftPopup.popUpView().showInViewController(self, completion: { (popUpView, sourceTextField, brandTextField, volumeTextField, membraneTextField, resorbableTag) in
                    popUpView.close()
                    self.surgicalRecordWisdom.isAllograft = true
                    self.surgicalRecordWisdom.allograftBrand = brandTextField.text!
                    self.surgicalRecordWisdom.allograftSource = sourceTextField.text!
                    self.surgicalRecordWisdom.allograftVolume = volumeTextField.text!
                    self.surgicalRecordWisdom.membrane = membraneTextField.text!
                    self.surgicalRecordWisdom.isResorbable = resorbableTag
                    
                })
            } else {
                self.surgicalRecordWisdom.isAllograft = false
                self.surgicalRecordWisdom.allograftBrand = ""
                self.surgicalRecordWisdom.allograftSource = ""
                self.surgicalRecordWisdom.allograftVolume = ""
            }
        } else if sender.tag == 3 {
            if sender.isSelected {
                ConnectivePopUp.popUpView().showInViewController(self, completion: { (popUpView, selectedIndex) in
                    popUpView.close()
                    self.surgicalRecordWisdom.isConnectiveTissue = true
                    self.surgicalRecordWisdom.isHarvestedFromPlate = selectedIndex
                })
            } else {
                self.surgicalRecordWisdom.isConnectiveTissue = false
                self.surgicalRecordWisdom.isHarvestedFromPlate = 0
            }
        }
    }
    
    @IBAction func radioBoneGraftAction() {
        if radioBoneGraft.isSelected {
            viewBoneGraft.isHidden = false
        } else {
            viewBoneGraft.isHidden = true
            
            buttonAutogenous.isSelected = true
            buttonAllograft.isSelected = true
            buttonConnective.isSelected = true
            
            buttonBoneGraftAction(buttonAutogenous)
            buttonBoneGraftAction(buttonAllograft)
            buttonBoneGraftAction(buttonConnective)
        }
    }
}
