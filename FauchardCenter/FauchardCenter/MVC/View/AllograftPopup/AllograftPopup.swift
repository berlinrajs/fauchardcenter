//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class AllograftPopup: UIView {
    
    class func popUpView() -> AllograftPopup {
        return Bundle.main.loadNibNamed("AllograftPopup", owner: nil, options: nil)!.first as! AllograftPopup
    }
    
    @IBOutlet weak var textField1: MCTextField!
    @IBOutlet weak var textField2: MCTextField!
    @IBOutlet weak var textField3: MCTextField!
    @IBOutlet weak var textField4: MCTextField!
    @IBOutlet weak var radioResorbable: RadioButton!
    
    var completion:((AllograftPopup, UITextField, UITextField, UITextField, UITextField, Int)->Void)?
    
    func showInViewController(_ viewController: UIViewController?, completion : @escaping (_ popUpView: AllograftPopup, _ sourceTextField: UITextField, _ brandTextField: UITextField, _ volumeTextField: UITextField, _ membraneTextField: UITextField , _ resorbableTag: Int) -> Void) {
        textField3.textFormat = .alphaNumeric
        
        self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        self.completion = completion
        if viewController != nil {
            viewController!.view.addSubview(self)
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        }
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionOK(_ sender: AnyObject) {
        completion?(self, self.textField1, self.textField2, self.textField3, self.textField4, self.radioResorbable.selected == nil ? 0 : self.radioResorbable.selected.tag)
    }
    func close() {
        self.removeFromSuperview()
    }
}
