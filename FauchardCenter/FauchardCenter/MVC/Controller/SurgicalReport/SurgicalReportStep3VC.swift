//
//  SurgicalReportStep3VC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 29/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalReportStep3VC: SurgicalReportVC {

    @IBOutlet weak var textFieldPlanIncludes: MCTextField!
    @IBOutlet weak var textFieldAnticipated: MCTextField!
    @IBOutlet weak var textViewComments: MCTextView!
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var labelDentistName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValues()
    }
    
    func loadValues() {
        textFieldPlanIncludes.text = surgicalReport.planIncludes
        textFieldAnticipated.text = surgicalReport.anticipated
        textViewComments.textValue = surgicalReport.comments
        
        labelDate.todayDate = patient.dateToday
        labelDentistName.text = patient.dentistName
    }
    
    func saveValues() {
        self.view.endEditing(true)
        surgicalReport.planIncludes = textFieldPlanIncludes.text!
        surgicalReport.anticipated = textFieldAnticipated.text!
        surgicalReport.comments = textViewComments.textValue!
        
        surgicalReport.dentistSignature = signatureView.signatureImage()
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonNextAction () {
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE TAP TO DATE")
        } else {
            saveValues()
            
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kSurgicalReportFormVC") as! SurgicalReportFormVC
            formVC.patient = self.patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
