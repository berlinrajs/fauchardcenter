//
//  NewPatient11ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/25/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient11ViewController: MCViewController {
    
    @IBOutlet weak var textfieldHeight : MCTextField!
    @IBOutlet weak var textfieldWeight : MCTextField!
    @IBOutlet weak var radioSmoke : RadioButton!
    @IBOutlet weak var viewWomens : UIView!
    @IBOutlet weak var radioBirthControl : RadioButton!
    @IBOutlet weak var radioPregnant : RadioButton!
    @IBOutlet weak var radioNursing : RadioButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        if patient.newPatient.gender == 1{
            viewWomens.isUserInteractionEnabled = false
            viewWomens.alpha = 0.5
        }
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadValue()  {
        textfieldHeight.textFormat = .alphaNumeric
        textfieldWeight.textFormat = .alphaNumeric
        textfieldHeight.setSavedText(patient.newPatient.height)
        textfieldWeight.setSavedText(patient.newPatient.weight)
        radioSmoke.setSelectedWithTag(patient.newPatient.smokeTag)
        radioBirthControl.setSelectedWithTag(patient.newPatient.birthControlTag)
        radioPregnant.setSelectedWithTag(patient.newPatient.pregnantTag)
        radioNursing.setSelectedWithTag(patient.newPatient.nursingTag)
        
    }
    
    func saveValue()  {
        patient.newPatient.height = textfieldHeight.getText()
        patient.newPatient.weight = textfieldWeight.getText()
        patient.newPatient.smokeTag = radioSmoke.selected == nil ? 0 : radioSmoke.selected.tag
        patient.newPatient.birthControlTag = radioBirthControl.selected == nil ? 0 : radioBirthControl.selected.tag
        patient.newPatient.pregnantTag = radioPregnant.selected == nil ? 0 : radioPregnant.selected.tag
        patient.newPatient.nursingTag = radioNursing.selected == nil ? 0 : radioNursing.selected.tag
        
        
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        self.view.endEditing(true)
        if (patient.newPatient.gender == 2 && (radioBirthControl.selected == nil || radioPregnant.selected == nil || radioNursing.selected == nil)) || radioSmoke.selected == nil{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else{
            saveValue()
            let new = newPatientStoryBoard.instantiateViewController(withIdentifier: "NewPatient12VC") as! NewPatient12ViewController
            new.patient = self.patient
            self.navigationController?.pushViewController(new, animated: true)

        }
    }
    
    @IBAction func onPregnantButtonPressed (withSender sender : RadioButton){
        if sender.tag == 1{
            PopupTextField.popUpView().showInViewController(self, WithTitle: "", placeHolder: "NUMBER OF WEEKS?", textFormat: TextFormat.numberCount3, completion: { (popUpView, textField) in
                if textField.isEmpty{
                    self.patient.newPatient.pregnantWeeks = "N/A"
                    sender.isSelected = false
                }else{
                    self.patient.newPatient.pregnantWeeks = textField.text!
                }
                popUpView.removeFromSuperview()
            })
        }else{
            self.patient.newPatient.pregnantWeeks = "N/A"
        }
    }
    
    @IBAction func onTobaccoButtonPressed (withSender sender : RadioButton){
        if sender.tag == 1{
            PopupTextFieldDual.popUpView().showInViewController(self,
                                                                WithTitle: (title1: "IF YES: HOW MUCH PER DAY?", title2: "FOR HOW LONG?"),
                                                                placeHolder: (placeholder1: "TYPE HERE", placeholder2: "TYPE HERE"),
                                                                textFormat: (format1: .alphaNumeric, format2: .default),
                                                                completion: { (popUpView, textField1, textField2)in
                if textField1.isEmpty && textField2.isEmpty{
                    self.patient.newPatient.pregnantWeeks = "N/A"
                    sender.isSelected = false
                }else{
                    self.patient.newPatient.howMuch = textField1.text!
                    self.patient.newPatient.howLong = textField2.text!
                }
                popUpView.removeFromSuperview()
            })
        }else{
            self.patient.newPatient.pregnantWeeks = "N/A"
        }
    }
    
    
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    


}
