//
//  SurgicalRecordWisdomStep1VC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 28/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalRecordWisdomStep1VC: SurgicalRecordWisdomVC {

    @IBOutlet weak var textFieldHeight: MCTextField!
    @IBOutlet weak var textFieldWeight: MCTextField!
    @IBOutlet weak var textFieldASA: MCTextField!
    @IBOutlet weak var textFieldNeedle: MCTextField!
    @IBOutlet var buttonSurgicalSiteOptions: [UIButton]!
    @IBOutlet weak var radioAllergies: RadioButton!
    
    var otherAllergies: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldWeight.textFormat = .alphaNumeric
        textFieldHeight.textFormat = .alphaNumeric
        loadValues()
    }
    
    func loadValues() {
        if self.surgicalRecordWisdom == nil {
            self.surgicalRecordWisdom = SurgicalRecordWisdom()
        }
        
        if surgicalRecordWisdom.surgicalSiteTags == nil {
            surgicalRecordWisdom.surgicalSiteTags = [Int]()
        }
        
        for button in buttonSurgicalSiteOptions {
            button.isSelected = surgicalRecordWisdom.surgicalSiteTags.contains(button.tag)
        }
        
        textFieldHeight.text = surgicalRecordWisdom.height
        textFieldWeight.text = surgicalRecordWisdom.weight
        textFieldASA.text = surgicalRecordWisdom.asaClassification
        textFieldNeedle.text = surgicalRecordWisdom.needleGuage
        radioAllergies.setSelectedWithTag(surgicalRecordWisdom.haveAllergiesTag == nil ? 0 : surgicalRecordWisdom.haveAllergiesTag)
        self.otherAllergies = surgicalRecordWisdom.otherAllergies
    }
    
    func saveValues() {
        self.view.endEditing(true)
        
        surgicalRecordWisdom.height = textFieldHeight.text!
        surgicalRecordWisdom.weight = textFieldWeight.text!
        surgicalRecordWisdom.asaClassification = textFieldASA.text!
        surgicalRecordWisdom.needleGuage = textFieldNeedle.text!
        surgicalRecordWisdom.haveAllergiesTag = radioAllergies.selected == nil ? 0 : radioAllergies.selected.tag
        self.surgicalRecordWisdom.otherAllergies = self.otherAllergies
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonNextAction() {
        saveValues()
            
        let step2VC = self.storyboard?.instantiateViewController(withIdentifier: "kSurgicalRecordWisdomStep2VC") as! SurgicalRecordWisdomStep2VC
        step2VC.patient = self.patient
        self.navigationController?.pushViewController(step2VC, animated: true)
    }
    
    @IBAction func buttonAllergiesAction () {
        if radioAllergies.isSelected {
            PopupTextView.popUpView().showWithPlaceHolder("PLEASE LIST HERE", completion: { (popUpView, textView) in
                popUpView.close()
                if textView.isEmpty {
                    self.otherAllergies = ""
                    self.radioAllergies.isSelected = false
                } else {
                    self.otherAllergies = textView.textValue
                }
            })
        } else {
            self.otherAllergies = ""
        }
    }
    
    @IBAction func buttonSurgicalSiteAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            if !surgicalRecordWisdom.surgicalSiteTags.contains(sender.tag) {
                surgicalRecordWisdom.surgicalSiteTags.append(sender.tag)
            }
        } else {
            if surgicalRecordWisdom.surgicalSiteTags.contains(sender.tag) {
                surgicalRecordWisdom.surgicalSiteTags.remove(at: surgicalRecordWisdom.surgicalSiteTags.index(of: sender.tag)!)
            }
        }
    }
}
