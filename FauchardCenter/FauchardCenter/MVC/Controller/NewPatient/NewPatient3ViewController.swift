//
//  NewPatient3ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/24/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient3ViewController: MCViewController {

    @IBOutlet weak var textfieldEmployerName : MCTextField!
    @IBOutlet weak var textfieldOccupation : MCTextField!
    @IBOutlet weak var textfieldEmployerPhone : MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        textfieldEmployerPhone.textFormat = .phone
        textfieldEmployerName.setSavedText(patient.newPatient.employerName)
        textfieldOccupation.setSavedText(patient.newPatient.occupation)
        textfieldEmployerPhone.setSavedText(patient.newPatient.employerPhone)
    }
    
    func saveValue()  {
        patient.newPatient.employerName = textfieldEmployerName.getText()
        patient.newPatient.occupation = textfieldOccupation.getText()
        patient.newPatient.employerPhone = textfieldEmployerPhone.getText()
        
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
     self.view.endEditing(true)
     if !textfieldEmployerPhone.isEmpty && !textfieldEmployerPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID EMPLOYER PHONE NUMBER")
        }else{
            saveValue()
            let new = newPatientStoryBoard.instantiateViewController(withIdentifier: "NewPatient4VC") as! NewPatient4ViewController
            new.patient = self.patient
            self.navigationController?.pushViewController(new, animated: true)
        }
    }
    
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }



}
