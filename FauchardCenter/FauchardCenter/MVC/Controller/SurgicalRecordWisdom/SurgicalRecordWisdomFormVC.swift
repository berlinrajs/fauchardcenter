//
//  SurgicalRecordWisdomFormVC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 28/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalRecordWisdomFormVC: SurgicalRecordWisdomVC {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDentistName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelHeightWeight: UILabel!
    @IBOutlet weak var labelASA: UILabel!
    @IBOutlet weak var labelNeedle: UILabel!
    
    @IBOutlet var buttonsSurgicalSite: [UIButton]!
    @IBOutlet weak var buttonAllergisNone: UIButton!
    @IBOutlet weak var labelAllergies: UILabel!
    @IBOutlet weak var radioEmotionalStatus: RadioButton!
    
    @IBOutlet weak var labelBPPre: UILabel!
    @IBOutlet weak var labelBPPost: UILabel!
    @IBOutlet weak var labelPulsePre: UILabel!
    @IBOutlet weak var labelPulsePost: UILabel!
    @IBOutlet weak var labelRespPre: UILabel!
    @IBOutlet weak var labelRespPost: UILabel!
    @IBOutlet weak var labelO2Pre: UILabel!
    @IBOutlet weak var labelO2Post: UILabel!
    
    @IBOutlet var buttonsDentalCodes: [UIButton]!
    @IBOutlet weak var radioSedation: RadioButton!
    @IBOutlet weak var labelTimeBilled: UILabel!
    @IBOutlet weak var labelVersed: UILabel!
    @IBOutlet weak var labelDemerol: UILabel!
    @IBOutlet weak var labelDexamethasone: UILabel!
    @IBOutlet weak var labelPhenegran: UILabel!
    @IBOutlet weak var labelNacran: UILabel!
    @IBOutlet weak var labelBenadryl: UILabel!
    @IBOutlet weak var labelFlumazenil: UILabel!
    @IBOutlet weak var labelDexatose: UILabel!
    
    @IBOutlet var buttonsLocalAnesthetic: [UIButton]!
    @IBOutlet var buttonsInsicions: [UIButton]!
   
    @IBOutlet weak var radioHardTissue: RadioButton!
    @IBOutlet weak var radioBiospy: RadioButton!
    @IBOutlet weak var buttonOccessous: UIButton!
    
    @IBOutlet weak var buttonAutoGenous: UIButton!
    @IBOutlet weak var labelAutoHarvested: UILabel!
    @IBOutlet weak var labelAutoVolume: UILabel!
    @IBOutlet weak var buttonAllograft: UIButton!
    @IBOutlet weak var labelAlloSource: UILabel!
    @IBOutlet weak var labelAlloBrand: UILabel!
    @IBOutlet weak var labelAlloVolume: UILabel!
    @IBOutlet weak var labelAlloMembrane: UILabel!
    @IBOutlet weak var radioResorbable: RadioButton!
    @IBOutlet weak var buttonConnextive: UIButton!
    @IBOutlet weak var radioHarvestedPlates: RadioButton!
    
    @IBOutlet weak var labelSuturesType: UILabel!
    @IBOutlet weak var labelSuturesNumber: UILabel!
    @IBOutlet weak var labelSuturesLocation: UILabel!
    
    @IBOutlet weak var radioPostOpIns: RadioButton!
    @IBOutlet weak var labelToWhom: UILabel!
    
    @IBOutlet weak var labelPreOPIns1: UILabel!
    @IBOutlet weak var labelPreOPIns2: UILabel!
    @IBOutlet weak var labelPreOPIns3: UILabel!
    @IBOutlet weak var radioHealingStent: RadioButton!
    @IBOutlet weak var radioBoneGraft: RadioButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        labelName.text = patient.fullName
        labelDentistName.text = patient.dentistName
        labelDate.text = patient.dateToday
        labelHeightWeight.text = surgicalRecordWisdom.height + "/" + surgicalRecordWisdom.weight
        labelASA.text = surgicalRecordWisdom.asaClassification
        labelNeedle.text = surgicalRecordWisdom.needleGuage
        
        for button in buttonsSurgicalSite {
            button.isSelected = surgicalRecordWisdom.surgicalSiteTags.contains(button.tag)
        }
        
        buttonAllergisNone.isSelected = surgicalRecordWisdom.haveAllergiesTag == nil ? false : surgicalRecordWisdom.haveAllergiesTag == 2
        labelAllergies.text = surgicalRecordWisdom.otherAllergies
        radioEmotionalStatus.setSelectedWithTag(surgicalRecordWisdom.emotionalStatusTag == nil ? 0 : surgicalRecordWisdom.emotionalStatusTag)
        
        labelBPPre.text = surgicalRecordWisdom.bpPreHigh + "/" + surgicalRecordWisdom.bpPreLow
        labelBPPost.text = surgicalRecordWisdom.bpPostHigh + "/" + surgicalRecordWisdom.bpPostLow
        labelPulsePre.text = surgicalRecordWisdom.pulsePreOP
        labelPulsePost.text = surgicalRecordWisdom.pulsePostOp
        labelRespPre.text = surgicalRecordWisdom.respPreOP
        labelRespPost.text = surgicalRecordWisdom.respPostOP
        labelO2Pre.text = surgicalRecordWisdom.o2PreOP
        labelO2Post.text = surgicalRecordWisdom.o2PostOP
        
        for button in buttonsDentalCodes {
            button.isSelected = surgicalRecordWisdom.dentalCodesTags.contains(button.tag)
        }
        
        radioSedation.setSelectedWithTag(surgicalRecordWisdom.sedationTag == nil ? 0 : surgicalRecordWisdom.sedationTag)
        labelTimeBilled.text = surgicalRecordWisdom.timeToBilled
        labelVersed.text = surgicalRecordWisdom.versed
        labelDemerol.text = surgicalRecordWisdom.demerol
        labelDexamethasone.text = surgicalRecordWisdom.dexamethasone
        labelPhenegran.text = surgicalRecordWisdom.phenegran
        labelNacran.text = surgicalRecordWisdom.narcan
        labelBenadryl.text = surgicalRecordWisdom.benadryl
        labelFlumazenil.text = surgicalRecordWisdom.flumazenil
        labelDexatose.text = surgicalRecordWisdom.dexamethasone
        
        for button in buttonsLocalAnesthetic {
            button.isSelected = surgicalRecordWisdom.localAnestheticTags.contains(button.tag)
        }
        for button in buttonsInsicions {
            button.isSelected = surgicalRecordWisdom.incisionsTags.contains(button.tag)
        }
        
        radioHardTissue.setSelectedWithTag(surgicalRecordWisdom.isHardTissue == nil ? 0 : surgicalRecordWisdom.isHardTissue)
        radioBiospy.setSelectedWithTag(surgicalRecordWisdom.isBiopsy == nil ? 0 : surgicalRecordWisdom.isBiopsy)
        buttonOccessous.isSelected = surgicalRecordWisdom.isOsseousWalls == nil ? false :surgicalRecordWisdom.isOsseousWalls == 1
        
        buttonAutoGenous.isSelected = surgicalRecordWisdom.isAutogenous
        labelAutoHarvested.text = surgicalRecordWisdom.autogenousHarvestedFrom
        labelAutoVolume.text = surgicalRecordWisdom.autogenousVolume
        buttonAllograft.isSelected = surgicalRecordWisdom.isAllograft
        labelAlloSource.text = surgicalRecordWisdom.allograftSource
        labelAlloBrand.text = surgicalRecordWisdom.allograftBrand
        labelAlloVolume.text = surgicalRecordWisdom.allograftVolume
        labelAlloMembrane.text = surgicalRecordWisdom.membrane
        radioResorbable.setSelectedWithTag(surgicalRecordWisdom.isResorbable == nil ? 0 : surgicalRecordWisdom.isResorbable)
        buttonConnextive.isSelected = surgicalRecordWisdom.isConnectiveTissue
        radioHarvestedPlates.setSelectedWithTag(surgicalRecordWisdom.isHarvestedFromPlate == nil ? 0 : surgicalRecordWisdom.isHarvestedFromPlate)
        
        labelSuturesType.text = surgicalRecordWisdom.suturesType
        labelSuturesNumber.text = surgicalRecordWisdom.suturesNumber
        labelSuturesLocation.text = surgicalRecordWisdom.suturesLocation
        
        radioPostOpIns.setSelectedWithTag(surgicalRecordWisdom.isGivenPostOpIns == nil ? 0 : surgicalRecordWisdom.isGivenPostOpIns)
        labelToWhom.text = surgicalRecordWisdom.givenToWhom
        
        surgicalRecordWisdom.preOpIns.setTextForArrayOfLabels([labelPreOPIns1, labelPreOPIns2, labelPreOPIns3])
        radioHealingStent.setSelectedWithTag(surgicalRecordWisdom.isHealingStentUsed == nil ? 0 : surgicalRecordWisdom.isHealingStentUsed)
        radioBoneGraft.setSelectedWithTag(surgicalRecordWisdom.isBoneCraft == nil ? 0 : surgicalRecordWisdom.isBoneCraft)
    }
}
