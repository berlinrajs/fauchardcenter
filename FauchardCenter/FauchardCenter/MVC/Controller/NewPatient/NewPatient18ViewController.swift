//
//  NewPatient18ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/28/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient18ViewController: MCViewController {
    
    @IBOutlet weak var textfieldMedicalDoctor : MCTextField!
    @IBOutlet weak var textviewOtherInformations : MCTextView!
    @IBOutlet weak var textviewLimitations : MCTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValue()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadValue()  {
        textviewOtherInformations.placeholder = "PLEASE TYPE HERE"
        textviewOtherInformations.text = patient.newPatient.otherInformations == "N/A" ? "PLEASE TYPE HERE" : patient.newPatient.otherInformations
        textviewOtherInformations.textColor =  patient.newPatient.otherInformations == "N/A" ? UIColor.lightGrayColor() : UIColor.blackColor()
        
        textviewLimitations.placeholder = "PLEASE TYPE HERE"
        textviewLimitations.text = patient.newPatient.limitations == "N/A" ? "PLEASE TYPE HERE" : patient.newPatient.limitations
        textviewLimitations.textColor =  patient.newPatient.limitations == "N/A" ? UIColor.lightGrayColor() : UIColor.blackColor()
        
        textfieldMedicalDoctor.setSavedText(patient.newPatient.medicalDoctor)
    }
    
    func saveValue()  {
        patient.newPatient.otherInformations = textviewOtherInformations.isEmpty ? "N/A" : textviewOtherInformations.text!
        patient.newPatient.limitations = textviewLimitations.isEmpty ? "N/A" : textviewLimitations.text!
        patient.newPatient.medicalDoctor = textfieldMedicalDoctor.getText()
        
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        self.view.endEditing(true)
        saveValue()
        let new = newPatientStoryBoard.instantiateViewControllerWithIdentifier("NewPatient19VC") as! NewPatient19ViewController
        new.patient = self.patient
        self.navigationController?.pushViewController(new, animated: true)
    }
    
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    
}
