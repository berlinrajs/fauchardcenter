//
//  MCPatient.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


class MCPatient: NSObject {
    
    required override init() {
        super.init()
    }
    
    init(forms : [Forms]) {
        super.init()
        self.selectedForms = forms
    }
    
    var selectedForms : [Forms]!
    var firstName : String!
    var lastName : String!
    var initial : String!
    
    var dateOfBirth : String!
    var dateToday : String!
    var dentistName: String!
    
//    var parentFirstName: String!
//    var parentLastName: String!
//    var parentDateOfBirth: String!
    
    var is18YearsOld : Bool {
        return patientAge >= 18
    }
    
    var fullName: String {
        return initial.characters.count > 0 ? firstName + " " + initial + " " + lastName : firstName + " " + lastName
    }
    var patientAge: Int! {
        get {
            if dateOfBirth == nil || dateOfBirth.characters.count == 0 {
                return 0
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = kCommonDateFormat
            
            let numberOfDays = (Date().timeIntervalSince(dateFormatter.date(from: self.dateOfBirth)!))/(3600 * 24)
            return Int(numberOfDays/365.2425)
        }
    }
    
//    var address: String!
//    var city: String!
//    var state: String!
//    var zip: String!
//    var phone: String!
//    var socialSecurityNumber: String!
//    var workPhone: String!
//    var cellPhone: String!
//    var email: String!
//    
//    var maritalStatus: Int!
//    var gender: Int!
//    var isEmployed: Bool!
//    var isResponsibleParty: Bool!
//    var havePrimaryInsurance: Bool!
//    var haveSecondaryInsurance: Bool!
//    
//    var employerName: String!
//    var employerPhone: String!
//    
//    var emergencyName: String!
//    var emergencyPhone: String!
//    
//    var responsibleRelation: Int!
//    var responsibleName: String!
//    var responsibleAddress: String!
//    var responsibleCity: String!
//    var responsibleState: String!
//    var responsibleZip: String!
//    var responsibleGroup: String!
//    var responsibleOtherRelation: String!
//    var signatureResponsible: UIImage!
//    
//    var primaryInsurance: Insurance!
//    var secondaryInsurance: Insurance!
    
    var visitorFirstName : String!
    var visitorLastName : String!
    
    var visitorFullName : String {
        return visitorFirstName + " " + visitorLastName
    }
//    var visitingPurpose : String!
//    
//    var signatureFinancialPolicy: UIImage!
//    var signatureAppointmentPolicy: UIImage!
//    var signatureReminderPolicy: UIImage!
//    
//    var reminderPhoneNumber: String!
//    var reminderEmail: String!
//    var isAllowText: Bool!
//    var isAllowEmail: Bool!
    
    var medical : MedicalHistory = MedicalHistory()
    
    var implantTreatments : [Int] = [Int]()
    var implantTretmentOther : String = "N/A"
    
    var newPatient : NewPatient = NewPatient()
    
    var surgicalRecord: SurgicalRecord!
    var surgicalRecordWisdom: SurgicalRecordWisdom!
    var surgicalReport: SurgicalReport!
    var treatmentReport: ImplantTreatmentReport!
    
    //crown Surgery
    var consentWitnessName : String = ""
    
    
}

class Insurance: NSObject {
    var insuranceName: String = "N/A"
    var subscriberName: String = "N/A"
    var DateOfBirth: String = "N/A"
    var socialSecurity: String = "N/A"
    var group: String = "N/A"
    var phoneNumber: String = "N/A"
}

class MedicalHistory : NSObject{
    
    var medicalQuestions : [[MCQuestion]]!
    
    var medications : String = "N/A"
    var physicianName : String = "N/A"
    var physicianPhone : String = "N/A"
    var patientSignature : UIImage!
    
    required override init() {
        super.init()
        let quest1: [String] = ["AIDS/HIV",
                                "Arthritis",
                                "Artificial Joints",
                                "Asthma/emphysema",
                                "Blood Disease",
                                "Cancer or tumors",
                                "Diabetes",
                                "Epilepsy",
                                "Excessive Bleeding",
                                "Fainting",
                                "Glaucoma",
                                "Head Injuries",
                                "Heart Disease"]
        
        let quest2: [String] = ["Hepatitis A, B, C",
                                "High Blood Pressure",
                                "Kidney Disease",
                                "Liver Disease",
                                "Low Blood Pressure",
                                "Mental Disorders",
                                "Osteoporosis",
                                "Pacemaker",
                                "Possibly pregnant now",
                                "Radiation Treatment",
                                "Sinus Surgery",
                                "Sleep Apnea",
                                "Stomach/GI Problems"]
        
        let quest3: [String] = ["Stroke",
                                "Thyroid Disease",
                                "Codeine Allergy",
                                "Latex Allergy",
                                "Penicillin Allergy",
                                "Sulfa Allergy",
                                "Other"]
        
        let quest4: [String] = ["Have you ever had any complications following medical or dental treatment?",
                                "Have you been admitted to a hospital or needed emergency care during the past two years?",
                                "Are you now under the care of a physician (other than for physicals)?",
                                "Do you have any health problems that need further clarification?",
                                "Do you require antibiotic pre-medication for cleaning visits or invasive dental treatment?",
                                "Are you allergic to any medications or products?"]

        self.medicalQuestions = [MCQuestion.arrayOfQuestions(quest1),MCQuestion.arrayOfQuestions(quest2),MCQuestion.arrayOfQuestions(quest3),MCQuestion.arrayOfQuestions(quest4)]
        self.medicalQuestions[2][6].isAnswerRequired = true
        self.medicalQuestions[3][0].isAnswerRequired = true
        self.medicalQuestions[3][1].isAnswerRequired = true
        self.medicalQuestions[3][2].isAnswerRequired = true
        self.medicalQuestions[3][3].isAnswerRequired = true
        self.medicalQuestions[3][5].isAnswerRequired = true
    }

}

class SurgicalRecord: NSObject {
    var treatmentTag: Int!
    var otherTreatment: String!
    var surgicalSiteTags: [Int]!
    var isAnterior: Int!
    var anatomicalLandmark: String!
    var haveAllergies: Int!
    var allergies: String!
    var emotionalStatusTag: Int!
    var bPpreOpHigh: String!
    var bPpreOpLow: String!
    var bPpostOpHigh: String!
    var bPpostOpLow: String!
    
    var pulsePreOp: String!
    var pulsePostOp: String!
    var localAnestheticTags: [Int]!
    var length1: String!
    var length2: String!
    var length3: String!
    var incisionsOther: String!
    var flapThickenssTag: Int!
    
    var graftType: Int!
    var OtherGraftType: String!
    var isBiopsy: Int!
    var isHardTissue: Int!
    var osseousWall: Int!
    
    var isAutogenous: Bool!
    var autogenousHarvestedFrom: String!
    var autogenousVolume: String!
    var isAllograft: Bool!
    var allograftSource: String!
    var allograftBrand: String!
    var allograftVolume: String!
    var membrane: String!
    var isResorbable: Int!
    var isConnectiveTissue: Bool!
    var isHarvestedFromPlate: Int!
    
    var suturesType: String!
    var suturesNumber: String!
    var suturesLocation: String!
    
    var isGivenPostOpIns: Int!
    var givenToWhom: String!
    var preOpIns: String!
}

class SurgicalRecordWisdom: NSObject {
    var surgicalSiteTags: [Int]!
    var height: String!
    var weight: String!
    var asaClassification: String!
    var needleGuage: String!
    var haveAllergiesTag: Int!
    var otherAllergies: String!
    
    var emotionalStatusTag: Int!
    var bpPreHigh: String!
    var bpPreLow: String!
    var bpPostHigh: String!
    var bpPostLow: String!
    var pulsePreOP: String!
    var pulsePostOp: String!
    var respPreOP: String!
    var respPostOP: String!
    var o2PreOP: String!
    var o2PostOP: String!
    
    var dentalCodesTags: [Int]!
    var sedationTag: Int!
    var timeToBilled: String!
    var versed: String!
    var demerol: String!
    var dexamethasone: String!
    var phenegran: String!
    var narcan: String!
    var benadryl: String!
    var flumazenil: String!
    var dextrose: String!
    
    var localAnestheticTags: [Int]!
    var incisionsTags: [Int]!
    var graftTypeTag: Int!
    var otherGraft: String!
    var isBiopsy: Int!
    var isHardTissue: Int!
    var isOsseousWalls: Int!
    
    var isBoneCraft: Int!
    var isAutogenous: Bool!
    var autogenousHarvestedFrom: String!
    var autogenousVolume: String!
    var isAllograft: Bool!
    var isConnectiveTissues: Bool!
    var allograftSource: String!
    var allograftBrand: String!
    var allograftVolume: String!
    var membrane: String!
    var isResorbable: Int!
    var isConnectiveTissue: Bool!
    var isHarvestedFromPlate: Int!
    
    var suturesType: String!
    var suturesNumber: String!
    var suturesLocation: String!
    
    var isGivenPostOpIns: Int!
    var givenToWhom: String!
    var preOpIns: String!
    
    var isHealingStentUsed: Int!
}

class SurgicalReport: NSObject {
    var reText: String!
    var dateOfSurgery: String!
    var typeOfSurgery: String!
    var flapSurgery: String!
    var boneGraftSurgery: String!
    
    var connectiveTissueGraft: String!
    var extraction: String!
    var biopsy: String!
    var rootAssumption: String!
    var crownLengthening: String!
    var ridgeAugmentation: String!
    
    var planIncludes: String!
    var anticipated: String!
    var comments: String!
    var dentistSignature: UIImage!
}

class ImplantTreatmentReport: NSObject {
    var implantSystem: String! = "NOBEL BIOCARE"
    var implantType: String! = "REPLACE WITH CONICAL CONNECTION"
    var immediateExtraction: String!
    var immediateTemporary: String!
    var bonyRidge: String!
    var dateOfUncovery: String!
    
    var finalRestoration: String!
    var treatmentNotes: [TreatmentNotes]!
    var signature: UIImage!
}

class NewPatient : NSObject{
    var title : Int = 0
    var maritalStatus : Int = 0
    var gender : Int = 0
    var legalTag : Int = 0
    var legalName : String = "N/A"
    var formerName : String = "N/A"
    
    
    var address : String = "N/A"
    var city : String = "N/A"
    var state : String = "N/A"
    var zipcode : String = "N/A"
    var postBox : String = "N/A"
    var socialSecurity : String = "N/A"
    var phoneNumber : String = "N/A"
    
    var employerName : String = "N/A"
    var occupation : String = "N/A"
    var employerPhone : String = "N/A"
    
    var dentalInsuranceTag : Int = 0
    var reference : Int = 0
    var referalDoctorName : String = "N/A"
    var referalOther : String = "N/A"
    
    var responsibleRelationship : Int = 0
    var responsibleName : String = "N/A"
    var responsibleBirthdate : String = "N/A"
    var responsibleAddress : String = "N/A"
    var responsiblePhoneNumber : String = "N/A"
    var responsiblePatientHere : Int = 0
    var responsibleRelationshipOther : String = "N/A"
    
    var responsibleEmployerName : String = "N/A"
    var responsibleOccupation : String = "N/A"
    var responsibleEmployerAddress : String = "N/A"
    var responsibleEmployerPhone : String = "N/A"
    var radioHasInsurance : Int = 0
    
    var primaryInsurance : Insurance = Insurance()
    var secondaryInsurance : Insurance = Insurance()
    
    var emergencyName : String = "N/A"
    var emergencyRelationship : String = "N/A"
    var emergencyHomePhone : String = "N/A"
    var emergencyWorkPhone : String = "N/A"

    var patientSignature1 : UIImage!
    var patientSignature2 : UIImage!
    var patientSignature3 : UIImage!
    var patientSignature4 : UIImage!
    
    var height : String = "N/A"
    var weight : String = "N/A"
    var smokeTag : Int = 0
    var pregnantTag : Int = 0
    var pregnantWeeks : String = "N/A"
    var birthControlTag : Int = 0
    var nursingTag : Int = 0
    var howMuch : String = "N/A"
    var howLong : String = "N/A"
    
    var medicalQuestions : [[MCQuestion]]!
    var allergies : [MCQuestion]!
    var medicalHistory : [[MCQuestion]]!
    
    var medications : String = "N/A"
    var extraProblems : String = "N/A"
    var concern : String = "N/A"
    var surgeryProcedures : String = "N/A"
    var mouthInjury : String = "N/A"
    
    
    var referringDentist : String = "N/A"
    var otherDentist : String = "N/A"
    var spouse : String = "N/A"
    var children : String = "N/A"
    var medicalDoctor : String = "N/A"
    var otherInformations : String = "N/A"
    var limitations : String = "N/A"
    
    var bp : String = "N/A"
    var heartRate : String = "N/A"
    
    var physicianName : String = "N/A"
    var physicianPhone : String = "N/A"
    var pharmacy : String = "N/A"
    var pharmacyPhone : String = "N/A"
    
    required override init() {
        super.init()
        let quest1: [String] = ["Abnormal Bleeding",
                                "Hemophilia",
                                "Anemia",
                                "Blood Transfusion",
                                "Alcohol Abuse",
                                "Heart Attack/Stroke",
                                "Angina Pectoris",
                                "Artificial Heart Valve",
                                "Heart Disease",
                                "Congenital Heart Defect",
                                "Mitral Valve Prolapse",
                                "Rheumatic Fever",
                                "Pace Maker",
                                "Low Blood Pressure"]
        
        let quest2: [String] = ["High Blood Pressure",
                                "Cholesterol",
                                "Sexually Transmitted Disease",
                                "Prosthetic Replacement",
                                "Seasonal Allergies",
                                "Asthma",
                                "Esophagitis",
                                "Difficulty Breathing",
                                "Arthritis/ Rheumatoid Osteoarthritis",
                                "Pneumocystis",
                                "Tuberculosis",
                                "Chemotherapy",
                                "Radiation Therapy",
                                "Cosmetic Surgery"]
        
        let quest3: [String] = ["Diabetes Type 1 Type 2",
                                "Drug Abuse",
                                "Epilepsy/Seizures",
                                "GERD/Acid Reflux",
                                "Fainting Spells",
                                "Fever Blisters",
                                "Frequent Headaches",
                                "Glaucoma",
                                "Hepatitis A, B, C",
                                "Benign Tumor or Growth",
                                "Cancer",
                                "HIV / AIDS",
                                "Kidney Problems",
                                "Liver Disease",
                                "Psychiatric Problems"]
        
        let quest4: [String] = ["Shingles",
                                "Sickle Cell Disease",
                                "Fibromyalgia",
                                "Sinus Problems",
                                "Sleep Apnea",
                                "Bruxism",
                                "Thyroid Problems",
                                "Ulcers",
                                "Venereal Disease",
                                "History of Bisphosphonates?",
                                "Osteoporosis",
                                "Xerostomia/Dry Mouth",
                                "Sjogrens Syndrome",
                                "Prostate Problems",
                                "Parkinson's Disease"]


        self.medicalQuestions = [MCQuestion.arrayOfQuestions(quest1),MCQuestion.arrayOfQuestions(quest2),MCQuestion.arrayOfQuestions(quest3),MCQuestion.arrayOfQuestions(quest4)]
        self.medicalQuestions[2][10].isAnswerRequired = true
        
        let quest5: [String] = ["Aspirin",
                                "Codeine",
                                "Dental Anesthetics",
                                "Erythromycin",
                                "Jewelry",
                                "Latex",
                                "Metals",
                                "Penicillin",
                                "Tetracycline",
                                "Other"
                                ]
        
        self.allergies = MCQuestion.arrayOfQuestions(quest5)
        self.allergies[9].isAnswerRequired = true
        
        let quest6: [String] = ["Hot or Cold?",
                                "Sweets",
                                "Biting or Chewing?",
                                "Have you noticed any mouth odors or bad taste?",
                                "Do you frequently get cold sores, blister or lesions?",
                                "Do your gums bleed or hurt?",
                                "Does food tend to become caught in between your teeth?",
                                "Have your parents experienced gum Disease or tooth loss?",
                                "Have you noticed any loose teeth or change in your bite?",
                                ]
        
        
        let quest8: [String] = ["Orthodontic treatment?",
                                "Oral Surgery",
                                "Periodontal Treatment?",
                                "A bite or mouth guard?",
                                "Clicking or popping of the jaw?",
                                "Clench or grind teeth while awake or asleep?",
                                "Difficulty in chewing on either side of the mouth?",
                                "Headaches, neck aches, or shoulder aches?",
                                "Snore or have any other sleeping disorders?"]

        


        
        self.medicalHistory = [MCQuestion.arrayOfQuestions(quest6),MCQuestion.arrayOfQuestions(quest8)]
        





    }

    
}

