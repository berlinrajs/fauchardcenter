//
//  Constants.swift
//  MConsentForms
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

public enum TextFormat : Int {
    case `default` = 0
    case socialSecurity
    case toothNumber
    case phone
    case extensionCode
    case zipcode
    case number
    case date
    case month
    case year
    case dateInCurrentYear
    case dateIn1980
    case time
    case middleInitial
    case state
    case amount
    case email
    case secureText
    case alphaNumeric
    case numberCount3
}

//KEYS
let kFormsCompletedNotification = "kAllFormsCompletedSuccessfully"
let kDateChangedNotification = "kDateChangedToNextDateNotificaion"

let kAppLoggedInKey = "kApploggedIn"
let kAppLoginUsernameKey = "kAppLoginUserName"
let kAppLoginPasswordKey = "kAppLoginPassword"

//VARIABLES
let kKeychainItemLoginName = "FauchardCenter: Google Login"
let kKeychainItemName = "FauchardCenter: Google Drive"
let kClientId = "912024721444-el6edd74h8oi3v257cm9ovquifet4h2t.apps.googleusercontent.com"
let kClientSecret = "wVvf0vfQRzF7homsDa10Kk04"
let kFolderName = "Fauchard Center"

let kDentistNames: [String] = ["DR. ERIK C. MATHYS, DDS, PC", "DR. JOSEPH N. DATAR DDS, MSD"]
let kDentistNameNeededForms = [kSurgicalForm, kSurgicalWTForm, kSurgicalReport, kImplantTreatmentReport, kTreatment]
let kClinicName = "Fauchard Center"
let kAppName = "Fauchard Center"
let kPlace = "GREENWOOD VILLAGE, CO"
let kState = "CO"
let kAppKey = "mcFauchard"
let kAppLoginAvailable: Bool = true
let kCommonDateFormat = "MMM dd, yyyy"

let kGoogleID = "demo@srswebsolutions.com"
let kGooglePassword = "Srsweb123#"
let kOutputFileMustBeImage: Bool = false
//END OF VARIABLES

//FORMS
let kNewPatientSignInForm = "NEW PATIENT SIGN-IN FORM"
let kMedicalHistory = "MEDICAL HISTORY UPDATE"
let kSurgicalForm = "SURGICAL RECORD FORM"
let kSurgicalWTForm = "SURGICAL RECORD FORM FOR WISDOM TEETH"
let kSurgicalReport = "SURGICAL REPORT FORM"
let kImplantTreatmentReport = "IMPLANT TREATMENT REPORT"

//CONSENT FORMS
let kConsentForms = "CONSENT FORMS"
let kCrownSurgery = "INFORMED CONSENT FOR CROWN LENGTHENING"
let kFrenulectomy = "INFORMED CONSENT FOR FRENECTOMY"
let kImplantSurgery = "INFORMED CONSENT FOR IMPLANT PLACEMENT"
let kSedation = "INFORMED CONSENT FOR INTRAVENOUS (IV) SEDATION"
let kOpenSinus = "PATIENT INFORMATION AND CONSENT FORM FOR OPEN SINUS ELEVATION SURGERY"
let kPeriodontal = "INFORMED CONSENT FOR PERIODONTAL SURGERY"
let kClosedSinus = "PATIENT INFORMATION AND CONSENT FORM FOR CLOSED SINUS ELEVATION SURGERY"
let kRidgeAugmented = "INFORMED CONSENT FOR RIDGE AUGMENTATION SURGERY"
let kTissueGrafting = "PATIENT INFORMATION AND CONSENT SOFT TISSUE GRAFTING"
let kRootPlaning = "INFORMED CONSENT FOR PERIOSCOPY AND SCALING AND ROOT PLANING"
let kTestimonial = "CONSENT FOR TESTIMONIAL"
let kToothRemoval = "INFORMED CONSENT FOR TOOTH EXTRACTION"
let kTreatment = "TREATMENT FORM"
let kPAOO = "INFORMED CONSENT FOR PERIODONTALLY ACCELERATED OSTEOGENIC ORTHODONTICS(PAOO)"
let kGingivialAugmentation = "INFORMED CONSENT FOR GINGIVIAL AUGMENTATION SURGERY"
let kSinusAugmentation = "INFORMED CONSENT FOR SINUS AUGMENTATION SURGERY"
let kImplantDebridmentConsent = "INFORMED CONSENT FOR IMPLANT DEBRIDEMENT AND BONE GRAFTING"

let kInsuranceCard = "SCAN INSURANCE CARD"
let kDrivingLicense = "SCAN DRIVING LICENSE"

let kFeedBack = "CUSTOMER REVIEW FORM"
let kVisitorCheckForm = "VISITOR CHECK IN FORM"
// END OF FORMS

let toothNumberRequired: [String] = [kCrownSurgery,kTissueGrafting,kImplantSurgery,kFrenulectomy,kToothRemoval,kImplantSurgery,kPAOO,kPeriodontal,kGingivialAugmentation,kRootPlaning,kRidgeAugmented,kSinusAugmentation]
//Replace '""' with form names
let consentIndex: Int = 8


