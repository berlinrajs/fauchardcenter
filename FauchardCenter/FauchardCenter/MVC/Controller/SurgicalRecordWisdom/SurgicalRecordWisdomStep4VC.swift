//
//  SurgicalRecordWisdomStep4VC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 29/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalRecordWisdomStep4VC: SurgicalRecordWisdomVC {
    
    @IBOutlet weak var radioSedation: RadioButton!
    @IBOutlet weak var textFieldTimeBilled: MCTextField!
    @IBOutlet var textFiedlVersed: MCTextField!
    @IBOutlet var textFiedlDemerol: MCTextField!
    @IBOutlet var textFiedlDexamethasone: MCTextField!
    @IBOutlet var textFiedlPhenegran: MCTextField!
    @IBOutlet var textFiedlNarcan: MCTextField!
    @IBOutlet var textFiedlBenadryl: MCTextField!
    @IBOutlet var textFiedlFlumazenil: MCTextField!
    @IBOutlet var textFiedlDextrose: MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldTimeBilled.textFormat = .time
        textFiedlVersed.textFormat = .alphaNumeric
        textFiedlDemerol.textFormat = .alphaNumeric
        textFiedlDexamethasone.textFormat = .alphaNumeric
        textFiedlPhenegran.textFormat = .alphaNumeric
        textFiedlNarcan.textFormat = .alphaNumeric
        textFiedlBenadryl.textFormat = .alphaNumeric
        textFiedlFlumazenil.textFormat = .alphaNumeric
        textFiedlDextrose.textFormat = .alphaNumeric
        
        loadValues()
    }
    
    func loadValues() {
        textFiedlVersed.text = surgicalRecordWisdom.versed
        textFiedlDemerol.text = surgicalRecordWisdom.demerol
        textFiedlDexamethasone.text = surgicalRecordWisdom.dexamethasone
        textFiedlPhenegran.text = surgicalRecordWisdom.phenegran
        textFiedlNarcan.text = surgicalRecordWisdom.narcan
        textFiedlBenadryl.text = surgicalRecordWisdom.benadryl
        textFiedlFlumazenil.text = surgicalRecordWisdom.flumazenil
        textFiedlDextrose.text = surgicalRecordWisdom.dextrose
        
        textFieldTimeBilled.text = surgicalRecordWisdom.timeToBilled
        radioSedation.setSelectedWithTag(surgicalRecordWisdom.sedationTag == nil ? 0 : surgicalRecordWisdom.sedationTag)
    }
    
    func saveValues() {
        self.view.endEditing(true)
        
        surgicalRecordWisdom.versed = textFiedlVersed.text!
        surgicalRecordWisdom.demerol = textFiedlDemerol.text!
        surgicalRecordWisdom.dexamethasone = textFiedlDexamethasone.text!
        surgicalRecordWisdom.phenegran = textFiedlPhenegran.text!
        surgicalRecordWisdom.narcan = textFiedlNarcan.text!
        surgicalRecordWisdom.benadryl = textFiedlBenadryl.text!
        surgicalRecordWisdom.flumazenil = textFiedlFlumazenil.text!
        surgicalRecordWisdom.dextrose = textFiedlDextrose.text!
        
        surgicalRecordWisdom.timeToBilled = textFieldTimeBilled.text!
        surgicalRecordWisdom.sedationTag = radioSedation.selected == nil ? 0 : radioSedation.selected.tag
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonNextAction() {
        saveValues()
        
        let step5VC = self.storyboard?.instantiateViewController(withIdentifier: "kSurgicalRecordWisdomStep5VC") as! SurgicalRecordWisdomStep5VC
        step5VC.patient = self.patient
        self.navigationController?.pushViewController(step5VC, animated: true)
    }
}
