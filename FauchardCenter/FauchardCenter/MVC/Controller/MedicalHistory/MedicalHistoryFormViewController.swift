//
//  MedicalHistoryFormViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/24/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistoryFormViewController: MCViewController {

    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet      var buttonsArray1 : [UIButton]!
    @IBOutlet      var buttonsArray2 : [UIButton]!
    @IBOutlet      var buttonsArray3 : [UIButton]!
    @IBOutlet      var buttonsArray4 : [RadioButton]!
    @IBOutlet      var labelOthers : [UILabel]!
    @IBOutlet weak var labelDentalTreatment : UILabel!
    @IBOutlet weak var labelEmergencyCare : UILabel!
    @IBOutlet weak var labelPhysicianCare : UILabel!
    @IBOutlet weak var labelPhysicianName : UILabel!
    @IBOutlet weak var labelPhysicianPhone : UILabel!
    @IBOutlet weak var labelHealthProblems : UILabel!
    @IBOutlet      var labelDosage : [UILabel]!
    @IBOutlet      var labelAllergyMedications : [UILabel]!
    @IBOutlet weak var imageviewPatientSign : UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelPatientName.text = patient.fullName
        labelDate.text = patient.dateToday
        labelDate1.text = patient.dateToday
        for btn in buttonsArray1{
            btn.isSelected = self.patient.medical.medicalQuestions[0][btn.tag].selectedOption == true
        }
        for btn in buttonsArray2{
            btn.isSelected = self.patient.medical.medicalQuestions[1][btn.tag].selectedOption == true
        }
        for btn in buttonsArray3{
            btn.isSelected = self.patient.medical.medicalQuestions[2][btn.tag].selectedOption == true
        }
        for btn in buttonsArray4{
            btn.isSelected = self.patient.medical.medicalQuestions[3][btn.tag].selectedOption == true
        }
        self.patient.medical.medicalQuestions[2][6].answer.setTextForArrayOfLabels(labelOthers)
        labelDentalTreatment.text = self.patient.medical.medicalQuestions[3][0].answer
        labelEmergencyCare.text = self.patient.medical.medicalQuestions[3][1].answer
        labelPhysicianCare.text = self.patient.medical.medicalQuestions[3][2].answer
        labelPhysicianName.text = patient.medical.physicianName
        labelPhysicianPhone.text = patient.medical.physicianPhone
        labelHealthProblems.text = self.patient.medical.medicalQuestions[3][3].answer
        patient.medical.medications.setTextForArrayOfLabels(labelDosage)
        self.patient.medical.medicalQuestions[3][5].answer.setTextForArrayOfLabels(labelAllergyMedications)
        imageviewPatientSign.image = patient.medical.patientSignature

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
