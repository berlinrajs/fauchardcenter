//
//  ImplantTreatmentReportFormVC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 30/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ImplantTreatmentReportFormVC: MCViewController {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate1: UILabel!
    @IBOutlet weak var labelDate2: UILabel!
    @IBOutlet weak var labelRestorativeDentist: UILabel!
    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var signatureView: UIImageView!
    
    @IBOutlet weak var labelImplantSystem: UILabel!
    @IBOutlet weak var labelImplantType: UILabel!
    @IBOutlet weak var labelImmediateExtraction: UILabel!
    @IBOutlet weak var labelTemporary: UILabel!
    @IBOutlet weak var labelBonyRidge: UILabel!
    @IBOutlet weak var labelDateUncovery: UILabel!
    @IBOutlet weak var labelFinalRestoration: UILabel!
    
    @IBOutlet var treatmentViews: [TreatmentNotesView]!
    
    var treatmentReport: ImplantTreatmentReport! {
        get {
            return patient.treatmentReport
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValues()
    }
    
    func loadValues() {
        
        for noteView in treatmentViews {
            if noteView.tag <= treatmentReport.treatmentNotes.count {
                noteView.configWithNotes(treatmentReport.treatmentNotes[noteView.tag - 1])
            }
        }
        
        labelName.text = patient.dentistName
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelRestorativeDentist.text = patient.dentistName
        labelPatientName.text = patient.fullName
        signatureView.image = treatmentReport.signature
        labelImplantSystem.text = treatmentReport.implantSystem
        labelImplantType.text = treatmentReport.implantType
        labelImmediateExtraction.text = treatmentReport.immediateExtraction
        labelTemporary.text = treatmentReport.immediateTemporary
        labelBonyRidge.text = treatmentReport.bonyRidge
        labelDateUncovery.text = treatmentReport.dateOfUncovery
        labelFinalRestoration.text = treatmentReport.finalRestoration
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

class TreatmentNotesView: UIView {
    @IBOutlet weak var labelLocation: UILabel!
    @IBOutlet weak var labelType: UILabel!
    @IBOutlet weak var labelLength: UILabel!
    @IBOutlet weak var labelHealing: UILabel!
    @IBOutlet weak var labelBoneType: UILabel!
    @IBOutlet weak var labelPlacement: UILabel!
    @IBOutlet weak var labelLotNumber: UILabel!
    
    func configWithNotes(_ note: TreatmentNotes) {
        labelLocation.text = note.location
        labelType.text = note.type
        labelLength.text = note.length
        labelHealing.text = note.healing
        labelBoneType.text = note.boneType
        labelPlacement.text = note.placement
        labelLotNumber.text = note.lotNumber
    }
}
