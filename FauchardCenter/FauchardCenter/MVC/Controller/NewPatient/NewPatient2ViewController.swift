//
//  NewPatient2ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/24/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient2ViewController: MCViewController {
    
    @IBOutlet weak var textfieldAddress : MCTextField!
    @IBOutlet weak var textfieldCity : MCTextField!
    @IBOutlet weak var textfieldState : MCTextField!
    @IBOutlet weak var textfieldZipcode : MCTextField!
    @IBOutlet weak var textfieldPostBox : MCTextField!
    @IBOutlet weak var textfieldSocialSecurity : MCTextField!
    @IBOutlet weak var textfieldPhoneNumber : MCTextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        textfieldState.textFormat = .state
        textfieldZipcode.textFormat = .zipcode
        textfieldSocialSecurity.textFormat = .socialSecurity
        textfieldPhoneNumber.textFormat = .phone
        textfieldAddress.setSavedText(patient.newPatient.address)
        textfieldCity.setSavedText(patient.newPatient.city)
        textfieldState.text = patient.newPatient.state == "N/A" ? kState : patient.newPatient.state
        textfieldZipcode.setSavedText(patient.newPatient.zipcode)
        textfieldPostBox.setSavedText(patient.newPatient.postBox)
        textfieldSocialSecurity.setSavedText(patient.newPatient.socialSecurity)
        textfieldPhoneNumber.setSavedText(patient.newPatient.phoneNumber)
    }
    
    func saveValue()  {
        patient.newPatient.address = textfieldAddress.getText()
        patient.newPatient.city = textfieldCity.getText()
        patient.newPatient.state = textfieldState.getText()
        patient.newPatient.zipcode = textfieldZipcode.getText()
        patient.newPatient.postBox = textfieldPostBox.getText()
        patient.newPatient.socialSecurity = textfieldSocialSecurity.getText()
        patient.newPatient.phoneNumber = textfieldPhoneNumber.getText()

    }

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        self.view.endEditing(true)
        if textfieldAddress.isEmpty || textfieldCity.isEmpty || textfieldState.isEmpty || textfieldZipcode.isEmpty{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if !textfieldZipcode.text!.isZipCode{
            self.showAlert("PLEASE ENTER THE VALID ZIPCODE")
        }else if !textfieldSocialSecurity.isEmpty && textfieldSocialSecurity.text!.characters.count != 9{
            self.showAlert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
        }else if !textfieldPhoneNumber.isEmpty && !textfieldPhoneNumber.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID PHONE NUMBER")
        }else{
            saveValue()
            let new = newPatientStoryBoard.instantiateViewController(withIdentifier: "NewPatient3VC") as! NewPatient3ViewController
            new.patient = self.patient
            self.navigationController?.pushViewController(new, animated: true)
        }
    }
    
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }

}
