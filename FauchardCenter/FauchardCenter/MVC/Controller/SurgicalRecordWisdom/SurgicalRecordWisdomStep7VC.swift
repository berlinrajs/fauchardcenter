//
//  SurgicalRecordWisdomStep7VC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 29/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalRecordWisdomStep7VC: SurgicalRecordWisdomVC {
    @IBOutlet weak var textFieldType: MCTextField!
    @IBOutlet weak var textFieldnumber: MCTextField!
    @IBOutlet weak var textFieldLocation: MCTextField!
    
    @IBOutlet weak var radioGivenIns: RadioButton!
    @IBOutlet weak var textViewPrescriptions: MCTextView!
    var towhom: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldLocation.textFormat = .alphaNumeric
        textFieldnumber.textFormat = .alphaNumeric
        
        loadValues()
    }
    
    func loadValues() {
        textFieldType.text = surgicalRecordWisdom.suturesType
        textFieldnumber.text = surgicalRecordWisdom.suturesNumber
        textFieldLocation.text = surgicalRecordWisdom.suturesLocation
        radioGivenIns.setSelectedWithTag(surgicalRecordWisdom.isGivenPostOpIns == nil ? 0 : surgicalRecordWisdom.isGivenPostOpIns)
        self.towhom = surgicalRecordWisdom.givenToWhom
        textViewPrescriptions.textValue = surgicalRecordWisdom.preOpIns
    }
    
    func saveValues() {
        self.view.endEditing(true)
        
        surgicalRecordWisdom.suturesType = textFieldType.text!
        surgicalRecordWisdom.suturesNumber = textFieldnumber.text!
        surgicalRecordWisdom.suturesLocation = textFieldLocation.text!
        surgicalRecordWisdom.isGivenPostOpIns = radioGivenIns.selected == nil ? 0 : radioGivenIns.selected.tag
        surgicalRecordWisdom.givenToWhom = self.towhom
        surgicalRecordWisdom.preOpIns = textViewPrescriptions.textValue!
    }
    
    @IBAction func radioGivenAction(_ sender: UIButton) {
        if radioGivenIns.isSelected {
            PopupTextField.popUpView().showInViewController(self, WithTitle: "GIVEN TO WHOM?", placeHolder: "NAME *", textFormat: TextFormat.default) { (popUpView, textField) in
                popUpView.close()
                if !textField.isEmpty {
                    self.towhom = textField.text!
                } else {
                    self.towhom = ""
                    self.radioGivenIns.isSelected = false
                }
            }
        } else {
            self.towhom = ""
        }
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonNextAction() {
        saveValues()
        let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kSurgicalRecordWisdomFormVC") as! SurgicalRecordWisdomFormVC
        formVC.patient = self.patient
        self.navigationController?.pushViewController(formVC, animated: true)
    }
}
