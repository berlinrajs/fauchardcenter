//
//  Testimonial1ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class Testimonial1ViewController: MCViewController {

    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelTitle : UILabel!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var signaturePatient : SignatureView!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate1.todayDate = patient.dateToday
        if patient.is18YearsOld{
            labelTitle.text = "Patient Signature"
            labelPatientName.text = patient.fullName
        }else{
            labelTitle.text = "Parent/Guardian Signature"
            labelPatientName.text = ""
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let test = consentStoryBoard.instantiateViewController(withIdentifier: "TestimonialFormVC") as! TestimonialFormViewController
            test.signPatient = signaturePatient.signatureImage()
            test.patient = self.patient
            self.navigationController?.pushViewController(test, animated: true)
            
        }
    }


}
