//
//  CustomAlert.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 19/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class CustomAlert: UIView {
    
    class func alertView() -> CustomAlert {
        return Bundle.main.loadNibNamed("CustomAlert", owner: nil, options: nil)!.first as! CustomAlert
    }
    
    @IBOutlet weak var labelTitle: UILabel!
    
    var completion:(()->Void)?
    var textFormat : TextFormat!
    var count : Int!
    
    func showWithTitle(_ title: String?, completion:@escaping (() -> Void)) {
        self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        labelTitle.text = title
        self.completion = completion
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.addSubview(self)
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonAction() {
        self.removeFromSuperview()
        completion?()
    }
}
