//
//  Treatment1ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/23/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class Treatment1ViewController: MCViewController {

    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelTitle : UILabel!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelFee : UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate1.todayDate = patient.dateToday
//        if patient.is18YearsOld{
            labelTitle.text = "Patient Signature"
            labelPatientName.text = patient.fullName
//        }else{
//            labelTitle.text = "Parent/Guardian Signature"
//            labelPatientName.text = ""
//        }
        
        labelFee.text = labelFee.text!.replacingOccurrences(of: "KFEE", with:         patient.selectedForms.first!.toothNumbers)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let treat = consentStoryBoard.instantiateViewController(withIdentifier: "TreatmentFormVC") as! TreatmentFormViewController
            treat.signPatient = signaturePatient.signatureImage()
            treat.patient = self.patient
            self.navigationController?.pushViewController(treat, animated: true)
            
        }
    }

}
