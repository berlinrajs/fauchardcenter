//
//  SurgicalRecordStep3VC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 25/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalRecordStep3VC: SurgicalRecordVC {

    @IBOutlet weak var textFieldPulsePre: MCTextField!
    @IBOutlet weak var textFieldPulsePost: MCTextField!
    
    @IBOutlet var buttonsAnesthetic: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textFieldPulsePre.setNumberFormatWithCount(3, limit: 0)
        textFieldPulsePost.setNumberFormatWithCount(3, limit: 0)
        
        loadValues()
    }
    
    func loadValues() {
        if surgicalRecord.localAnestheticTags == nil {
            surgicalRecord.localAnestheticTags = [Int]()
        }
        
        textFieldPulsePre.text = surgicalRecord.pulsePreOp
        textFieldPulsePost.text = surgicalRecord.pulsePostOp
        
        for button in buttonsAnesthetic {
            button.isSelected = surgicalRecord.localAnestheticTags.contains(button.tag)
        }
    }
    
    @IBAction func buttonAnestheticAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            if !surgicalRecord.localAnestheticTags.contains(sender.tag) {
                surgicalRecord.localAnestheticTags.append(sender.tag)
            }
        } else {
            if surgicalRecord.localAnestheticTags.contains(sender.tag) {
                surgicalRecord.localAnestheticTags.remove(at: surgicalRecord.localAnestheticTags.index(of: sender.tag)!)
            }
        }
    }
    
    func saveValues() {
        self.view.endEditing(true)
        
        surgicalRecord.pulsePreOp = textFieldPulsePre.text!
        surgicalRecord.pulsePostOp = textFieldPulsePost.text!
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonNextAction() {
        saveValues()
        let step4VC = self.storyboard?.instantiateViewController(withIdentifier: "kSurgicalRecordStep4VC") as! SurgicalRecordStep4VC
        step4VC.patient = self.patient
        self.navigationController?.pushViewController(step4VC, animated: true)
    }
}
