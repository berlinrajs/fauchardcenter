//
//  SurgicalReportVC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 29/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalReportVC: MCViewController {
    var surgicalReport: SurgicalReport! {
        get {
            return patient.surgicalReport
        } set {
            patient.surgicalReport = newValue
        }
    }
}
