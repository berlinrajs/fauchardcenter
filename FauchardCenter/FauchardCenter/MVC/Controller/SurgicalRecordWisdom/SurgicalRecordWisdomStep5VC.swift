//
//  SurgicalRecordWisdomStep5VC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 29/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalRecordWisdomStep5VC: SurgicalRecordWisdomVC {
    
    @IBOutlet weak var dropDownGraftType: BRDropDown!
    @IBOutlet var buttonIncisions: [UIButton]!
    @IBOutlet var buttonsAnesthetic: [UIButton]!
    
    var otherGraftType: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        dropDownGraftType.items = ["Sinus", "Block", "Particulate mg", "Socket", "Ridge Split", "Other"]
//        dropDownGraftType.placeholder = "-- SELECT --"
//        dropDownGraftType.delegate = self
        
        loadValues()
    }
    
    func loadValues() {
//        dropDownGraftType.selectedIndex = surgicalRecordWisdom.graftTypeTag == nil ? 0 : surgicalRecordWisdom.graftTypeTag
//        self.otherGraftType = surgicalRecordWisdom.otherGraft
        
        if surgicalRecordWisdom.incisionsTags == nil {
            surgicalRecordWisdom.incisionsTags = [Int]()
        }
        if surgicalRecordWisdom.localAnestheticTags == nil {
            surgicalRecordWisdom.localAnestheticTags = [Int]()
        }
        
        for button in buttonIncisions {
            button.isSelected = self.surgicalRecordWisdom.incisionsTags.contains(button.tag)
        }
        
        for button in buttonsAnesthetic {
            button.isSelected = self.surgicalRecordWisdom.localAnestheticTags.contains(button.tag)
        }
    }
    
    func saveValues() {
        self.view.endEditing(true)
        
//        surgicalRecordWisdom.graftTypeTag = dropDownGraftType.selectedIndex
//        surgicalRecordWisdom.otherGraft = self.otherGraftType
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonNextAction() {
        saveValues()
        
        let step6VC = self.storyboard?.instantiateViewController(withIdentifier: "kSurgicalRecordWisdomStep6VC") as! SurgicalRecordWisdomStep6VC
        step6VC.patient = self.patient
        self.navigationController?.pushViewController(step6VC, animated: true)
    }
    
    @IBAction func buttonAnestheticAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            if !surgicalRecordWisdom.localAnestheticTags.contains(sender.tag) {
                surgicalRecordWisdom.localAnestheticTags.append(sender.tag)
            }
        } else {
            if surgicalRecordWisdom.localAnestheticTags.contains(sender.tag) {
                surgicalRecordWisdom.localAnestheticTags.remove(at: surgicalRecordWisdom.localAnestheticTags.index(of: sender.tag)!)
            }
        }
    }
    
    @IBAction func buttonIncisionAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            if !surgicalRecordWisdom.incisionsTags.contains(sender.tag) {
                surgicalRecordWisdom.incisionsTags.append(sender.tag)
            }
        } else {
            if surgicalRecordWisdom.incisionsTags.contains(sender.tag) {
                surgicalRecordWisdom.incisionsTags.remove(at: surgicalRecordWisdom.incisionsTags.index(of: sender.tag)!)
            }
        }
    }
}

//extension SurgicalRecordWisdomStep5VC: BRDropDownDelegate {
//    func dropDown(dropDown: BRDropDown, selectedAtIndex index: Int, selectedOption option: String?) {
//        if index == 6 {
//            PopupTextField.popUpView().showInViewController(self, WithTitle: "BONE GRAFT TYPE", placeHolder: "TYPE HERE", textFormat: .Default, completion: { (popUpView, textField) in
//                popUpView.close()
//                if textField.isEmpty {
//                    self.otherGraftType = ""
//                    dropDown.reset()
//                } else {
//                    self.otherGraftType = textField.text!
//                }
//            })
//        } else {
//            self.otherGraftType = ""
//        }
//    }
//}
