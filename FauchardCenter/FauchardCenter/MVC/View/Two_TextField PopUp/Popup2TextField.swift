//
//  PopupTextField.swift
//  FusionDental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class Popup2TextField: UIView {

//    static var sharedInstance : Popup2TextField! {
//        get {
//            return NSBundle.mainBundle().loadNibNamed("Popup2TextField", owner: nil, options: nil)!.first as! Popup2TextField
//        }
//    }
    
    class func popUpView() -> Popup2TextField {
        return Bundle.main.loadNibNamed("Popup2TextField", owner: nil, options: nil)!.first as! Popup2TextField
    }
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    
    @IBOutlet weak var textField: MCTextField!
    
     @IBOutlet weak var textField1: MCTextField!
    
    @IBOutlet weak var labelTitle: UILabel!
    
    var completion:((Popup2TextField, UITextField,UITextField, Bool)->Void)?
    var textFormat : TextFormat!
    var count : Int!
    
    
//    func show(completion : (textField : UITextField,UITextField, isEdited : Bool) -> Void) {
//        self.showWithTitle("TYPE HERE", placeHolder1: "TYPE HERE", textFormat: .Default,textFormat1: .Default, completion: completion)
//    }
    
    func showWithTitle(_ viewController: UIViewController? ,placeHolder: String?, placeHolder1 : String?, textFormat: TextFormat,textFormat1: TextFormat, completion : @escaping (_ popUpView : Popup2TextField, _ textField : UITextField,_ textField1 : UITextField, _ isEdited : Bool) -> Void) {
        textField.text = ""
        textField1.text = ""
        
        self.textField.textFormat = textFormat
        self.textField1.textFormat = textFormat1
        
        if let _ = placeHolder {
            textField.placeholder = placeHolder
            textField1.placeholder = placeHolder1
        }
        
        self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        self.completion = completion
        if viewController == nil {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        } else {
            viewController?.view.addSubview(self)
        }
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
       
    }
    
    
    func showWithLabels(_ viewController: UIViewController?,labelValue1: String?,labelValue2:String?, placeHolder: String?, placeHolder1 : String?, textFormat: TextFormat,textFormat1: TextFormat, completion : @escaping (_ popUpView : Popup2TextField,_ textField : UITextField,_ textField1 : UITextField, _ isEdited : Bool) -> Void) {
        textField.text = ""
        textField1.text = ""
        
        self.label1.text = labelValue1
        self.label2.text = labelValue2
        self.label1.isHidden = false
        self.label2.isHidden = false
        
        self.textField.textFormat = textFormat
        //        self.textField.prepareTextField()
        self.textField1.textFormat = textFormat1
        //        self.textField1.prepareTextField()
        
        if let _ = placeHolder {
            textField.placeholder = placeHolder
            textField1.placeholder = placeHolder1
        }
        
        self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        self.completion = completion
        if viewController == nil{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.addSubview(self)
        }else{
        
        viewController?.view.addSubview(self)
        }
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
      
    @IBAction func buttonActionOK(_ sender: AnyObject) {
        //self.removeFromSuperview()
        completion?(self,self.textField,self.textField1, isEdited)
    }
    
    var isEdited : Bool {
        get {
            return !textField.isEmpty && !textField1.isEmpty
        }
    }
}
