//
//  NewPatient19ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/28/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient19ViewController: MCViewController {

    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var textviewLimitations : MCTextView!
     @IBOutlet weak var textviewOtherInformations : MCTextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        loadValue()
    }
    func loadValue()  {
        textviewOtherInformations.placeholder = "PLEASE TYPE HERE"
        textviewOtherInformations.text = patient.newPatient.otherInformations == "N/A" ? "PLEASE TYPE HERE" : patient.newPatient.otherInformations
        textviewOtherInformations.textColor =  patient.newPatient.otherInformations == "N/A" ? UIColor.lightGray : UIColor.black
        
        textviewLimitations.placeholder = "PLEASE TYPE HERE"
        textviewLimitations.text = patient.newPatient.limitations == "N/A" ? "PLEASE TYPE HERE" : patient.newPatient.limitations
        textviewLimitations.textColor =  patient.newPatient.limitations == "N/A" ? UIColor.lightGray : UIColor.black
        
        
    }
    func saveValue()  {
        patient.newPatient.otherInformations = textviewOtherInformations.isEmpty ? "N/A" : textviewOtherInformations.text!
        patient.newPatient.limitations = textviewLimitations.isEmpty ? "N/A" : textviewLimitations.text!
        
        
    }
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        self.view.endEditing(true)
        if !signaturePatient.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValue()
            patient.newPatient.patientSignature3 = signaturePatient.signatureImage()
            
        let new = newPatientStoryBoard.instantiateViewController(withIdentifier: "NewPatientFormVC") as! NewPatientFormViewController
        new.patient = self.patient
        self.navigationController?.pushViewController(new, animated: true)
        }
    }



}
