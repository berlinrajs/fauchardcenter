//
//  PhysicianViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/29/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PhysicianViewController: MCViewController {

    @IBOutlet weak var textfieldPhysicianName : MCTextField!
    @IBOutlet weak var textfieldPhysicianPhone : MCTextField!
    @IBOutlet weak var textfieldPharmacy : MCTextField!
    @IBOutlet weak var textfieldPharmacyPhone : MCTextField!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadValue()  {
        labelDate.todayDate = patient.dateToday
        textfieldPhysicianPhone.textFormat = .phone
        textfieldPharmacyPhone.textFormat = .phone
        textfieldPhysicianName.setSavedText(patient.newPatient.physicianName)
        textfieldPhysicianPhone.setSavedText(patient.newPatient.physicianPhone)
        textfieldPharmacy.setSavedText(patient.newPatient.pharmacy)
        textfieldPharmacyPhone.setSavedText(patient.newPatient.pharmacyPhone)
    }
    
    func saveValue()  {
        patient.newPatient.physicianName = textfieldPhysicianName.getText()
        patient.newPatient.physicianPhone = textfieldPhysicianPhone.getText()
        patient.newPatient.pharmacy = textfieldPharmacy.getText()
        patient.newPatient.pharmacyPhone = textfieldPharmacyPhone.getText()
    }


    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        self.view.endEditing(true)
        if !textfieldPhysicianPhone.isEmpty && !textfieldPhysicianPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID PHYSICIAN PHONE NUMBER")
        }else if !textfieldPharmacyPhone.isEmpty && !textfieldPharmacyPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID PHARMACY PHONE NUMBER")
        }else if !signaturePatient.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            patient.newPatient.patientSignature2 = signaturePatient.signatureImage()
            saveValue()
            let new = newPatientStoryBoard.instantiateViewController(withIdentifier: "NewPatient19VC") as! NewPatient19ViewController
            new.patient = self.patient
            self.navigationController?.pushViewController(new, animated: true)
        }
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }


}
