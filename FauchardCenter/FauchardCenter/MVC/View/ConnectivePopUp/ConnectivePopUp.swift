//
//  ConnectivePopUp.swift
//  FusionDental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ConnectivePopUp: UIView {
    
    class func popUpView() -> ConnectivePopUp {
        return Bundle.main.loadNibNamed("ConnectivePopUp", owner: nil, options: nil)!.first as! ConnectivePopUp
    }
    
    @IBOutlet weak var radioOption: RadioButton!
    var completion:((ConnectivePopUp, Int)->Void)?
    
    func showInViewController(_ viewController: UIViewController?, completion : @escaping (_ popUpView: ConnectivePopUp, _ selectedIndex: Int) -> Void) {
        
        self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        self.completion = completion
        if viewController != nil {
            viewController!.view.addSubview(self)
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        }
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionOK(_ sender: AnyObject) {
        completion?(self, self.radioOption.selected == nil ? 0 : radioOption.selected.tag)
    }
    
    func close() {
        self.removeFromSuperview()
    }
}
