//
//  CrownSurgery1ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/21/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class CrownSurgery1ViewController: MCViewController {

    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelTitle : UILabel!
    @IBOutlet weak var labelToothNumber : UILabel!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var labelDate2 : DateLabel!
    @IBOutlet weak var textfieldWitnessName : MCTextField!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var signatureWitness : SignatureView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        if patient.is18YearsOld{
            labelTitle.text = "Patient Signature"
            labelPatientName.text = patient.fullName
        }else{
            labelTitle.text = "Guardian, or Authorized Representative Signature"
            labelPatientName.text = ""
        }
        
        let form : Forms = patient.selectedForms.first!
        labelToothNumber.text = labelToothNumber.text?.replacingOccurrences(of: "kToothNumber", with: form.toothNumbers)

    }
    func saveValues()
    {
        patient.consentWitnessName = textfieldWitnessName.text!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        self.view.endEditing(true)
        if !signaturePatient.isSigned() || !signatureWitness.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped || !labelDate2.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValues()
            let crown = consentStoryBoard.instantiateViewController(withIdentifier: "CrownSurgeryFormVC") as! CrownSurgeryFormViewController
            crown.signPatient = signaturePatient.signatureImage()
            crown.signWitness = signatureWitness.signatureImage()
            crown.patient = self.patient
            self.navigationController?.pushViewController(crown, animated: true)

        }
    }


}
