//
//  SurgicalRecordStep5VC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 25/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalRecordStep5VC: SurgicalRecordVC {
    
    @IBOutlet weak var radioBiopsy: RadioButton!
    @IBOutlet weak var radioTissue: RadioButton!
    @IBOutlet weak var radioOsseous: RadioButton!
    
    @IBOutlet weak var buttonAllograft: UIButton!
    @IBOutlet weak var buttonAutogenous: UIButton!
    @IBOutlet weak var buttonConnective: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValues()
    }
    
    func loadValues() {
        radioBiopsy.setSelectedWithTag(surgicalRecord.isBiopsy == nil ? 0 : surgicalRecord.isBiopsy)
        radioTissue.setSelectedWithTag(surgicalRecord.isHardTissue == nil ? 0 : surgicalRecord.isHardTissue)
        radioOsseous.setSelectedWithTag(surgicalRecord.osseousWall == nil ? 0 : surgicalRecord.osseousWall)
        
        buttonAllograft.isSelected = surgicalRecord.isAllograft == nil ? false : surgicalRecord.isAllograft
        buttonAutogenous.isSelected = surgicalRecord.isAutogenous == nil ? false : surgicalRecord.isAutogenous
        buttonConnective.isSelected = surgicalRecord.isConnectiveTissue == nil ? false : surgicalRecord.isConnectiveTissue
    }
    
    func saveValues() {
        self.view.endEditing(true)
        
        surgicalRecord.isBiopsy = radioBiopsy.selected == nil ? 0 : radioBiopsy.selected.tag
        surgicalRecord.isHardTissue = radioTissue.selected == nil ? 0 : radioTissue.selected.tag
        surgicalRecord.osseousWall = radioOsseous.selected == nil ? 0 : radioOsseous.selected.tag
        
        surgicalRecord.isAllograft = buttonAllograft.isSelected
        surgicalRecord.isAutogenous = buttonAutogenous.isSelected
        surgicalRecord.isConnectiveTissue = buttonConnective.isSelected
    }
    
    @IBAction func buttonBoneGraftAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.tag == 1 {
            if sender.isSelected {
                AutogenousPopup.popUpView().showInViewController(self, completion: { (popUpView, harvestedFromTextField, volumeTextField) in
                    popUpView.close()
                    self.surgicalRecord.isAutogenous = true
                    self.surgicalRecord.autogenousHarvestedFrom = harvestedFromTextField.text!
                    self.surgicalRecord.autogenousVolume = volumeTextField.text!
                })
            } else {
                self.surgicalRecord.isAutogenous = false
                self.surgicalRecord.autogenousHarvestedFrom = ""
                self.surgicalRecord.autogenousVolume = ""
            }
        } else if sender.tag == 2 {
            if sender.isSelected {
                AllograftPopup.popUpView().showInViewController(self, completion: { (popUpView, sourceTextField, brandTextField, volumeTextField, membraneTextField, resorbableTag) in
                    popUpView.close()
                    self.surgicalRecord.isAllograft = true
                    self.surgicalRecord.allograftBrand = brandTextField.text!
                    self.surgicalRecord.allograftSource = sourceTextField.text!
                    self.surgicalRecord.allograftVolume = volumeTextField.text!
                    self.surgicalRecord.membrane = membraneTextField.text!
                    self.surgicalRecord.isResorbable = resorbableTag
                    
                })
            } else {
                self.surgicalRecord.isAllograft = false
                self.surgicalRecord.allograftBrand = ""
                self.surgicalRecord.allograftSource = ""
                self.surgicalRecord.allograftVolume = ""
            }
        } else if sender.tag == 3 {
            if sender.isSelected {
                ConnectivePopUp.popUpView().showInViewController(self, completion: { (popUpView, selectedIndex) in
                    popUpView.close()
                    self.surgicalRecord.isConnectiveTissue = true
                    self.surgicalRecord.isHarvestedFromPlate = selectedIndex
                })
            } else {
                self.surgicalRecord.isConnectiveTissue = false
                self.surgicalRecord.isHarvestedFromPlate = 0
            }
        }
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonNextAction() {
        saveValues()
        
        let step6VC = self.storyboard?.instantiateViewController(withIdentifier: "kSurgicalRecordStep6VC") as! SurgicalRecordStep6VC
        step6VC.patient = self.patient
        self.navigationController?.pushViewController(step6VC, animated: true)
    }
}
