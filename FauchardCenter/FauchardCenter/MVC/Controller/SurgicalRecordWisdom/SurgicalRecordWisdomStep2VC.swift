//
//  SurgicalRecordWisdomStep2VC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 28/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalRecordWisdomStep2VC: SurgicalRecordWisdomVC {

    @IBOutlet weak var dropDownEmotional: BRDropDown!
    @IBOutlet weak var textFieldPreBPHigh: MCTextField!
    @IBOutlet weak var textFieldPreBPLow: MCTextField!
    
    @IBOutlet weak var textFieldPostBpHigh: MCTextField!
    @IBOutlet weak var textFieldPostBpLow: MCTextField!
    @IBOutlet weak var textFieldPrePulse: MCTextField!
    @IBOutlet weak var textFieldPostPulse: MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textFieldPreBPLow.setNumberFormatWithCount(3, limit: 0)
        textFieldPreBPHigh.setNumberFormatWithCount(3, limit: 0)
        textFieldPostBpLow.setNumberFormatWithCount(3, limit: 0)
        textFieldPostBpHigh.setNumberFormatWithCount(3, limit: 0)
        textFieldPrePulse.setNumberFormatWithCount(3, limit: 0)
        textFieldPostPulse.setNumberFormatWithCount(3, limit: 0)
        
        dropDownEmotional.items = ["Calm", "Cooperative", "Nervous", "Agitated"]
        dropDownEmotional.placeholder = "-- SELECT --"
        
        loadValues()
    }
    
    func loadValues() {
        dropDownEmotional.selectedIndex = surgicalRecordWisdom.emotionalStatusTag == nil ? 0 : surgicalRecordWisdom.emotionalStatusTag
        textFieldPreBPHigh.text = surgicalRecordWisdom.bpPreHigh
        textFieldPreBPLow.text = surgicalRecordWisdom.bpPreLow
        textFieldPostBpHigh.text = surgicalRecordWisdom.bpPostHigh
        textFieldPostBpLow.text = surgicalRecordWisdom.bpPostLow
        textFieldPrePulse.text = surgicalRecordWisdom.pulsePreOP
        textFieldPostPulse.text = surgicalRecordWisdom.pulsePostOp
    }
    
    func saveValues() {
        self.view.endEditing(true)
        
        surgicalRecordWisdom.emotionalStatusTag = dropDownEmotional.selectedIndex
        surgicalRecordWisdom.bpPreHigh = textFieldPreBPHigh.text!
        surgicalRecordWisdom.bpPreLow = textFieldPreBPLow.text!
        surgicalRecordWisdom.bpPostHigh = textFieldPostBpHigh.text!
        surgicalRecordWisdom.bpPostLow = textFieldPostBpLow.text!
        surgicalRecordWisdom.pulsePreOP = textFieldPrePulse.text!
        surgicalRecordWisdom.pulsePostOp = textFieldPostPulse.text!
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonNextAction() {
        saveValues()
        let step3VC = self.storyboard?.instantiateViewController(withIdentifier: "kSurgicalRecordWisdomStep3VC") as! SurgicalRecordWisdomStep3VC
        step3VC.patient = self.patient
        self.navigationController?.pushViewController(step3VC, animated: true)
    }
}
