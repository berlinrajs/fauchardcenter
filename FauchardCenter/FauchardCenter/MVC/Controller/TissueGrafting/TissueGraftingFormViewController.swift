//
//  TissueGraftingFormViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class TissueGraftingFormViewController: MCViewController {

    var signPatient : UIImage!
    var signWitness : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var imageviewPatientSign : UIImageView!
    @IBOutlet weak var imageviewWitnessSign : UIImageView!
    @IBOutlet weak var labelToothNumber : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelPatientName.text = patient.fullName
        labelDate.text = patient.dateToday
        imageviewPatientSign.image = signPatient
        imageviewWitnessSign.image = signWitness
        let form : Forms = patient.selectedForms.first!
        labelToothNumber.text = form.toothNumbers

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
