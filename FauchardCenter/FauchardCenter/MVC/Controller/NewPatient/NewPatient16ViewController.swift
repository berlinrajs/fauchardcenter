//
//  NewPatient16ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient16ViewController: MCViewController {

    @IBOutlet weak var textviewMouthInjury : MCTextView!
    @IBOutlet weak var textviewSurgeryProcedures : MCTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadValue()  {
        textviewMouthInjury.placeholder = "PLEASE TYPE HERE"
        textviewMouthInjury.text = patient.newPatient.mouthInjury == "N/A" ? "PLEASE TYPE HERE" : patient.newPatient.mouthInjury
        textviewMouthInjury.textColor =  patient.newPatient.mouthInjury == "N/A" ? UIColor.lightGray : UIColor.black
        
        textviewSurgeryProcedures.placeholder = "PLEASE TYPE HERE"
        textviewSurgeryProcedures.text = patient.newPatient.surgeryProcedures == "N/A" ? "PLEASE TYPE HERE" : patient.newPatient.surgeryProcedures
        textviewSurgeryProcedures.textColor =  patient.newPatient.surgeryProcedures == "N/A" ? UIColor.lightGray : UIColor.black
        
        
        
    }
    
    func saveValue()  {
        patient.newPatient.mouthInjury = textviewMouthInjury.isEmpty ? "N/A" : textviewMouthInjury.text!
        
        patient.newPatient.surgeryProcedures = textviewSurgeryProcedures.isEmpty ? "N/A" : textviewSurgeryProcedures.text!
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        self.view.endEditing(true)
       
            saveValue()
            
            let new = newPatientStoryBoard.instantiateViewController(withIdentifier: "PhysicianVC") as! PhysicianViewController
            new.patient = self.patient
            self.navigationController?.pushViewController(new, animated: true)

        
    }
    
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    

}
