//
//  ImplantTreatmentReportStep1VCViewController.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 30/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ImplantTreatmentReportStep1VC: MCViewController {

    @IBOutlet weak var textFieldImplantSystem: MCTextField!
    @IBOutlet weak var textFieldImplantType: MCTextField!
    @IBOutlet weak var textFieldImmediateExtractions: MCTextField!
    @IBOutlet weak var textFieldImmediateTemporary: MCTextField!
    @IBOutlet weak var textFieldBonyRidge: MCTextField!
    @IBOutlet weak var textFieldDateUnCovery: MCTextField!
    
    var treatmentReport: ImplantTreatmentReport! {
        get {
            return patient.treatmentReport
        } set {
            patient.treatmentReport = newValue
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.treatmentReport == nil {
            self.treatmentReport = ImplantTreatmentReport()
        }
        
        textFieldDateUnCovery.textFormat = .dateInCurrentYear
        
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        textFieldImplantSystem.text = treatmentReport.implantSystem
        textFieldImplantType.text = treatmentReport.implantType
        textFieldImmediateExtractions.text = treatmentReport.immediateExtraction
        textFieldImmediateTemporary.text = treatmentReport.immediateTemporary
        textFieldBonyRidge.text = treatmentReport.bonyRidge
        textFieldDateUnCovery.text = treatmentReport.dateOfUncovery
    }
    
    func saveValues() {
        treatmentReport.implantSystem = textFieldImplantSystem.text!
        treatmentReport.implantType = textFieldImplantType.text!
        treatmentReport.immediateExtraction = textFieldImmediateExtractions.text!
        treatmentReport.immediateTemporary = textFieldImmediateTemporary.text!
        treatmentReport.bonyRidge = textFieldBonyRidge.text!
        treatmentReport.dateOfUncovery = textFieldDateUnCovery.text!
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonNextAction() {
        saveValues()
        
        let step2VC = self.storyboard?.instantiateViewController(withIdentifier: "kImplantTreatmentReportStep2VC") as! ImplantTreatmentReportStep2VC
        step2VC.patient = self.patient
        self.navigationController?.pushViewController(step2VC, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
