//
//  AutogenousPopup.swift
//  FusionDental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class AutogenousPopup: UIView {
    
    class func popUpView() -> AutogenousPopup {
        return Bundle.main.loadNibNamed("AutogenousPopup", owner: nil, options: nil)!.first as! AutogenousPopup
    }
    
    @IBOutlet weak var textField1: MCTextField!
    @IBOutlet weak var textField2: MCTextField!
    
    var completion:((AutogenousPopup, UITextField, UITextField)->Void)?
    
    func showInViewController(_ viewController: UIViewController?, completion : @escaping (_ popUpView: AutogenousPopup, _ harvestedFromTextField: UITextField, _ volumeTextField: UITextField) -> Void) {
        self.textField2.textFormat = .alphaNumeric
        
        self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        self.completion = completion
        if viewController != nil {
            viewController!.view.addSubview(self)
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        }
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionOK(_ sender: AnyObject) {
        completion?(self, self.textField1, self.textField2)
    }
    
    func close() {
        self.removeFromSuperview()
    }
}
