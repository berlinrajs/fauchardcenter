//
//  NewPatient6ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/24/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient6ViewController: MCViewController {
    
    @IBOutlet weak var textfieldEmployerName : MCTextField!
    @IBOutlet weak var textfieldOccupation : MCTextField!
    @IBOutlet weak var textfieldAddress : MCTextField!
    @IBOutlet weak var textfieldPhone : MCTextField!
    @IBOutlet weak var radioInsurance : RadioButton!
    var isSelfSelected : Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loadValue()  {
        if isSelfSelected == true && patient.newPatient.responsibleEmployerName == "N/A"{
            textfieldEmployerName.setSavedText(patient.newPatient.employerName)
            textfieldOccupation.setSavedText(patient.newPatient.occupation)
//            textfieldAddress.setSavedText(patient.newPatient.address)
            textfieldPhone.setSavedText(patient.newPatient.employerPhone)
        }else{
            textfieldPhone.textFormat = .phone
            textfieldEmployerName.setSavedText(patient.newPatient.responsibleEmployerName)
            textfieldOccupation.setSavedText(patient.newPatient.responsibleOccupation)
            textfieldAddress.setSavedText(patient.newPatient.responsibleEmployerAddress)
            textfieldPhone.setSavedText(patient.newPatient.responsibleEmployerPhone)
            radioInsurance.setSelectedWithTag(patient.newPatient.radioHasInsurance)
            
        }
        
    }
    
    func saveValue()  {
        patient.newPatient.responsibleEmployerName = textfieldEmployerName.getText()
        patient.newPatient.responsibleEmployerAddress = textfieldAddress.getText()
        patient.newPatient.responsibleOccupation = textfieldOccupation.getText()
        patient.newPatient.responsibleEmployerPhone = textfieldPhone.getText()
        patient.newPatient.radioHasInsurance = radioInsurance.selected == nil ? 0 : radioInsurance.selected.tag
        
        
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        self.view.endEditing(true)
        if radioInsurance.selected == nil {
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if !textfieldPhone.isEmpty && !textfieldPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID EMPLOYER PHONE NUMBER")
        }else{
            saveValue()
            if radioInsurance.selected.tag == 1{
                let new = newPatientStoryBoard.instantiateViewController(withIdentifier: "NewPatient7VC") as! NewPatient7ViewController
                new.patient = self.patient
                self.navigationController?.pushViewController(new, animated: true)
                
            }else{
                let new = newPatientStoryBoard.instantiateViewController(withIdentifier: "NewPatient9VC") as! NewPatient9ViewController
                new.patient = self.patient
                self.navigationController?.pushViewController(new, animated: true)

            }
            
        }
    }
    
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    
    
}
