//
//  ImplantDebridementConsentFormVC.swift
//  FauchardCenter
//
//  Created by Manjusha Chembra on 7/31/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import Foundation

class ImplantDebridementConsentFormVC: MCViewController {
    
    var signPatient : UIImage!
    var signWitness : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var imageviewPatientSign : UIImageView!
    @IBOutlet weak var imageviewWitnessSign : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelWitnessName : UILabel!
    @IBOutlet weak var labelImplantNumber : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelPatientName.text = patient.fullName
        labelDate.text = patient.dateToday
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        imageviewPatientSign.image = signPatient
        imageviewWitnessSign.image = signWitness 
        labelWitnessName.text = patient.consentWitnessName
        let form : Forms = patient.selectedForms.first!
        labelImplantNumber.text = form.toothNumbers
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
