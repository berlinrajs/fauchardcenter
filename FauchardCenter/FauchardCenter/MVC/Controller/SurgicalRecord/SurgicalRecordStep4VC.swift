//
//  SurgicalRecordStep4VC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 25/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalRecordStep4VC: SurgicalRecordVC {

    @IBOutlet weak var dropDownGraftType: BRDropDown!
    @IBOutlet weak var textFieldLength1: MCTextField!
    @IBOutlet weak var textFieldLength2: MCTextField!
    @IBOutlet weak var textFieldLength3: MCTextField!
    @IBOutlet weak var textFieldOther: MCTextField!
    
    @IBOutlet weak var radioThickness: RadioButton!
    
    var otherGraftType: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dropDownGraftType.items = ["Sinus", "Block", "Particulate mg", "Socket", "Ridge Split", "Other"]
        dropDownGraftType.placeholder = "-- SELECT --"
        dropDownGraftType.delegate = self
        
        textFieldLength1.textFormat = .alphaNumeric
        textFieldLength2.textFormat = .alphaNumeric
        textFieldLength3.textFormat = .alphaNumeric
        
        loadValues()
    }
    
    func loadValues() {
        
        dropDownGraftType.selectedIndex = surgicalRecord.graftType == nil ? 0 : surgicalRecord.graftType
        
        textFieldLength1.text = surgicalRecord.length1
        textFieldLength2.text = surgicalRecord.length2
        textFieldLength3.text = surgicalRecord.length3
        textFieldOther.text = surgicalRecord.incisionsOther
        self.otherGraftType = surgicalRecord.OtherGraftType
        
        radioThickness.setSelectedWithTag(surgicalRecord.flapThickenssTag == nil ? 0 : surgicalRecord.flapThickenssTag)
    }
    
    func saveValues() {
        self.view.endEditing(true)
        
        surgicalRecord.graftType = dropDownGraftType.selectedIndex
        surgicalRecord.length1 = textFieldLength1.text!
        surgicalRecord.length2 = textFieldLength2.text!
        surgicalRecord.length3 = textFieldLength3.text!
        surgicalRecord.incisionsOther = textFieldOther.text!
        surgicalRecord.OtherGraftType = self.otherGraftType
        
        surgicalRecord.flapThickenssTag = radioThickness.selected == nil ? 0 : radioThickness.selected.tag
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonNextAction() {
        saveValues()
        let step5VC = self.storyboard?.instantiateViewController(withIdentifier: "kSurgicalRecordStep5VC") as! SurgicalRecordStep5VC
        step5VC.patient = self.patient
        self.navigationController?.pushViewController(step5VC, animated: true)
    }
}

extension SurgicalRecordStep4VC: BRDropDownDelegate {
    func dropDown(_ dropDown: BRDropDown, selectedAtIndex index: Int, selectedOption option: String?) {
        if index == 6 {
            PopupTextField.popUpView().showInViewController(self, WithTitle: "BONE GRAFT TYPE", placeHolder: "TYPE HERE", textFormat: .default, completion: { (popUpView, textField) in
                popUpView.close()
                if textField.isEmpty {
                    self.otherGraftType = ""
                    dropDown.reset()
                } else {
                    self.otherGraftType = textField.text!
                }
            })
        } else {
            self.otherGraftType = ""
        }
    }
}
