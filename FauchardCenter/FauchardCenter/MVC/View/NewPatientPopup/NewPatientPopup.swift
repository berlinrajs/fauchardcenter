//
//  NewPatientPopup.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/28/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatientPopup: UIView {

    class func popUpView() -> NewPatientPopup {
        return Bundle.main.loadNibNamed("NewPatientPopup", owner: nil, options: nil)!.first as! NewPatientPopup
    }
    
    @IBOutlet weak var textFieldHighBp: MCTextField!
    @IBOutlet weak var textFieldLowBp: MCTextField!
    @IBOutlet weak var textfieldHeartRate : MCTextField!
    
    var completion:((NewPatientPopup, String, String)->Void)?
    var errorCompletion : ((String)->Void)?


    func showInViewController(_ viewController: UIViewController?, completion : @escaping (_ popUpView: NewPatientPopup, _ bp : String, _ heartRate : String) -> Void, errorCompletion : @escaping (_ errorMessage : String) -> Void) {
        textFieldHighBp.text = ""
        textFieldLowBp.text = ""
        textfieldHeartRate.text = ""
        textfieldHeartRate.setNumberFormatWithCount(3, limit: 0)
        textFieldHighBp.setNumberFormatWithCount(3, limit: 0)
        textFieldLowBp.setNumberFormatWithCount(3, limit: 0)
        
        self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        self.completion = completion
        self.errorCompletion = errorCompletion
        if viewController != nil {
            viewController!.view.addSubview(self)
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        }
        
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionOK(_ sender: AnyObject) {
        if (textFieldHighBp.isEmpty && !textFieldLowBp.isEmpty) || (!textFieldHighBp.isEmpty && textFieldLowBp.isEmpty){
            errorCompletion!("PLEASE ENTER THE VALID BLOOD PRESSURE")
        }else{
            let bp : String = textFieldHighBp.isEmpty ? "N/A" : textFieldHighBp.text! + " over " + textFieldLowBp.text!
            completion?(self, bp, textfieldHeartRate.isEmpty ? "N/A" : textfieldHeartRate.text!)
        }
        
    }
    func close() {
        self.removeFromSuperview()
    }

}
