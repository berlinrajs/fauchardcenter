//
//  TreatmentFormViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/23/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class TreatmentFormViewController: MCViewController {

    var signPatient : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var imageviewPatientSign : UIImageView!
    @IBOutlet weak var labelPatientName1 : UILabel!
    @IBOutlet weak var labelDoctorName : UILabel!
    @IBOutlet weak var labelCaseFee : UILabel!
    @IBOutlet var labelArrayComments: [FormLabel]!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelPatientName.text = patient.fullName
        labelDate.text = patient.dateToday
        imageviewPatientSign.image = signPatient
        labelPatientName1.text = patient.fullName
        labelDoctorName.text = patient.dentistName
        labelCaseFee.text = "Case Fee is a total of \(patient.selectedForms.first!.toothNumbers) in this office."
        
        patient.selectedForms.first!.treatmentComments.setTextForArrayOfLabels(labelArrayComments)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
