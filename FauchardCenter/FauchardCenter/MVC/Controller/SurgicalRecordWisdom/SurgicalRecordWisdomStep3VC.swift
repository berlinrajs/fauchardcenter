//
//  SurgicalRecordWisdomStep3VC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 28/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalRecordWisdomStep3VC: SurgicalRecordWisdomVC {

    @IBOutlet weak var textFieldRespPreOP: MCTextField!
    @IBOutlet weak var textFieldRespPostOP: MCTextField!
    @IBOutlet weak var textFieldO2PreOp: MCTextField!
    @IBOutlet weak var textFieldO2PostOP: MCTextField!
    @IBOutlet var buttonsDentalCodes: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textFieldO2PreOp.setNumberFormatWithCount(3, limit: 100)
        textFieldO2PostOP.setNumberFormatWithCount(3, limit: 100)
        textFieldRespPreOP.setNumberFormatWithCount(3, limit: 0)
        textFieldRespPostOP.setNumberFormatWithCount(3, limit: 0)
        
        loadValues()
    }
    
    func loadValues() {
        if surgicalRecordWisdom.dentalCodesTags == nil {
            surgicalRecordWisdom.dentalCodesTags = [Int]()
        }
        
        textFieldRespPreOP.text = surgicalRecordWisdom.respPreOP
        textFieldRespPostOP.text = surgicalRecordWisdom.respPostOP
        textFieldO2PreOp.text = surgicalRecordWisdom.o2PreOP
        textFieldO2PostOP.text = surgicalRecordWisdom.o2PostOP
        
        for button in buttonsDentalCodes {
            button.isSelected = surgicalRecordWisdom.dentalCodesTags.contains(button.tag)
        }
    }
    
    @IBAction func buttonDentalCodeAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            if !surgicalRecordWisdom.dentalCodesTags.contains(sender.tag) {
                surgicalRecordWisdom.dentalCodesTags.append(sender.tag)
            }
        } else {
            if surgicalRecordWisdom.dentalCodesTags.contains(sender.tag) {
                surgicalRecordWisdom.dentalCodesTags.remove(at: surgicalRecordWisdom.dentalCodesTags.index(of: sender.tag)!)
            }
        }
    }
    
    func saveValues() {
        self.view.endEditing(true)
        surgicalRecordWisdom.respPreOP = textFieldRespPreOP.text!
        surgicalRecordWisdom.respPostOP = textFieldRespPostOP.text!
        surgicalRecordWisdom.o2PreOP = textFieldO2PreOp.text!
        surgicalRecordWisdom.o2PostOP = textFieldO2PostOP.text!
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonNextAction() {
        saveValues()
        let step4VC = self.storyboard?.instantiateViewController(withIdentifier: "kSurgicalRecordWisdomStep4VC") as! SurgicalRecordWisdomStep4VC
        step4VC.patient = self.patient
        self.navigationController?.pushViewController(step4VC, animated: true)
    }
}
