//
//  NewPatient8ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/25/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient8ViewController: MCViewController {

    @IBOutlet weak var textfieldInsuranceName : MCTextField!
    @IBOutlet weak var textfieldSubscriberName : MCTextField!
    @IBOutlet weak var textfieldDOB : MCTextField!
    @IBOutlet weak var textfieldSSN : MCTextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        textfieldDOB.textFormat = .dateIn1980
        textfieldSSN.textFormat = .socialSecurity
        textfieldInsuranceName.setSavedText(patient.newPatient.secondaryInsurance.insuranceName)
        textfieldSubscriberName.setSavedText(patient.newPatient.secondaryInsurance.subscriberName)
        textfieldDOB.setSavedText(patient.newPatient.secondaryInsurance.DateOfBirth)
        textfieldSSN.setSavedText(patient.newPatient.secondaryInsurance.socialSecurity)
        
    }
    
    func saveValue()  {
        patient.newPatient.secondaryInsurance.insuranceName = textfieldInsuranceName.getText()
        patient.newPatient.secondaryInsurance.subscriberName = textfieldSubscriberName.getText()
        patient.newPatient.secondaryInsurance.DateOfBirth = textfieldDOB.getText()
        patient.newPatient.secondaryInsurance.socialSecurity = textfieldSSN.getText()
        
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        self.view.endEditing(true)
        if !textfieldSSN.isEmpty && textfieldSSN.text!.characters.count != 9 {
            self.showAlert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
        }else{
            saveValue()
            let new = newPatientStoryBoard.instantiateViewController(withIdentifier: "NewPatient9VC") as! NewPatient9ViewController
            new.patient = self.patient
            self.navigationController?.pushViewController(new, animated: true)
            
        }
    }
    
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }



}
