//
//  NewPatient4ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/24/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient4ViewController: MCViewController {

    @IBOutlet weak var dropDownReference  : BRDropDown!
    @IBOutlet weak var radioHaveDentalInsurance : RadioButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadValue()  {
        dropDownReference.placeholder = "-- SELECT --"
        dropDownReference.items = ["DR","INSURANCE PLAN","FAMILY","FRIEND","INTERNET SEARCH","OTHER"]
        dropDownReference.delegate = self
        dropDownReference.selectedIndex = patient.newPatient.reference
        radioHaveDentalInsurance.setSelectedWithTag(patient.newPatient.dentalInsuranceTag)
        
    }
    
    func saveValue()  {
        
        patient.newPatient.reference = dropDownReference.selectedOption == nil ? 0 : dropDownReference.selectedIndex
        
        patient.newPatient.dentalInsuranceTag = radioHaveDentalInsurance.selected.tag
        
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        self.view.endEditing(true)
        if radioHaveDentalInsurance.selected == nil
        {
           self.showAlert("PLEASE SELECT IF YOU HAVE DENTAL INSURANCE OR NOT")
        }
        else
        {
            
            saveValue()
            
            if radioHaveDentalInsurance.selected.tag == 1
            {
                let new = newPatientStoryBoard.instantiateViewController(withIdentifier: "NewPatient5VC") as! NewPatient5ViewController
                new.patient = self.patient
                self.navigationController?.pushViewController(new, animated: true)
            }
            else
            {
                let new = newPatientStoryBoard.instantiateViewController(withIdentifier: "NewPatient9VC") as! NewPatient9ViewController
                new.patient = self.patient
                self.navigationController?.pushViewController(new, animated: true)
            }
            
        }
        
    }
    
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
}

extension NewPatient4ViewController : BRDropDownDelegate{
    func dropDown(_ dropDown: BRDropDown, selectedAtIndex index: Int, selectedOption option: String?) {
        if index == 1 {
            PopupTextField.popUpView().showInViewController(self, WithTitle: "PLEASE ENTER THE DOCTOR NAME", placeHolder: "DOCTOR NAME", textFormat: TextFormat.default, completion: { (popUpView, textField) in
                if textField.isEmpty{
                    self.patient.newPatient.referalDoctorName = "N/A"
                    self.patient.newPatient.referalOther = "N/A"
                }else{
                    self.patient.newPatient.referalDoctorName = textField.text!
                    self.patient.newPatient.referalOther = "N/A"
                }
                popUpView.removeFromSuperview()
            })
        }else if index == 6{
            PopupTextField.popUpView().showInViewController(self, WithTitle: "", placeHolder: "PLEASE SPECIFY", textFormat: TextFormat.default, completion: { (popUpView, textField) in
                if textField.isEmpty{
                    self.patient.newPatient.referalDoctorName = "N/A"
                    self.patient.newPatient.referalOther = "N/A"
                }else{
                    self.patient.newPatient.referalDoctorName = "N/A"
                    self.patient.newPatient.referalOther = textField.text!
                }
                popUpView.removeFromSuperview()
            })

        }else{
            self.patient.newPatient.referalDoctorName = "N/A"
            self.patient.newPatient.referalOther = "N/A"

        }
    }
}
