//
//  NewPatient13ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/25/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient13ViewController: MCViewController {

    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var buttonVerified: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    
    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        self.view.endEditing(true)
        if buttonVerified.isSelected == false {
            self.showAlert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        } else {
            let new = newPatientStoryBoard.instantiateViewController(withIdentifier: "NewPatient14VC") as! NewPatient14ViewController
            new.patient = self.patient
            self.navigationController?.pushViewController(new, animated: true)
        }
    }
    
    
}

extension NewPatient13ViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.newPatient.allergies.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PatientInfoCell
        cell.delegate = self
        cell.tag = indexPath.row
        cell.configCell(self.patient.newPatient.allergies[indexPath.row])
        
        return cell
    }
}

extension NewPatient13ViewController : PatientInfoCellDelegate{
    
    func radioButtonTappedForCell(_ cell: PatientInfoCell) {
       
            PopupTextView.sharedInstance.showWithPlaceHolder("TYPE HERE") { (popUpView, textView) in
                if !textView.isEmpty{
                    self.patient.newPatient.allergies[cell.tag].answer = textView.text!
                }else{
                    cell.radioButtonYes.isSelected = false
                    self.patient.newPatient.allergies[cell.tag].selectedOption = false
                }
                popUpView.removeFromSuperview()
            } 
                
    }
    
}
