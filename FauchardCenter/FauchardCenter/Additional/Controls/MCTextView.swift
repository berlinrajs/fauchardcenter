//
//  MCTextView.swift
//  ProDental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class MCTextView: UITextView {
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
        
        self.autocorrectionType = UITextAutocorrectionType.no
        self.spellCheckingType = UITextSpellCheckingType.no
        self.autocapitalizationType = UITextAutocapitalizationType.allCharacters
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        if delegate == nil {
            delegate = self
        }
    }
    
    var borderColor: UIColor = UIColor.white {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    var cornerRadius: CGFloat = 3.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }
    
    var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    var textValue: String? {
        get {
            self.delegate?.textViewDidBeginEditing?(self)
            let returnString = self.text
            self.delegate?.textViewDidEndEditing?(self)
            return returnString
        } set {
            self.delegate?.textViewDidBeginEditing?(self)
            self.text = newValue
            self.delegate?.textViewDidEndEditing?(self)
        }
    }
    
    @IBInspectable var characterCount: Int! = 0
    
    @IBInspectable var placeholder: String? = nil {
        didSet {
            if placeholder?.characters.count > 0 {
                self.text = placeholder
                self.textColor = self.placeholderColor
            }
        }
    }
    @IBInspectable var defaultTextColor: UIColor = UIColor.black {
        didSet {
            if self.text != self.placeholder {
                self.textColor = defaultTextColor
            }
        }
    }
    
    @IBInspectable var placeholderColor: UIColor = UIColor.lightGray {
        didSet {
            if self.text == self.placeholder {
                self.textColor = placeholderColor
            }
        }
    }
    
    override var isEmpty : Bool {
        return text == placeholder ? true : super.isEmpty
    }
}
extension MCTextView: UITextViewDelegate {
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = placeholder
            textView.textColor = placeholderColor
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: text)
        
        return characterCount == 0 || newString.characters.count <= characterCount
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == placeholder {
            textView.text = ""
            textView.textColor = self.defaultTextColor
        }
    }
}
