//
//  AllograftPopup.swift
//  FusionDental
//
//  TreatmentNotePopUp.swift
//  FusionDental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class TreatmentNotePopUp: UIView {
    
    class func popUpView() -> TreatmentNotePopUp {
        return Bundle.main.loadNibNamed("TreatmentNotePopUp", owner: nil, options: nil)!.first as! TreatmentNotePopUp
    }
    
    @IBOutlet weak var textFieldLocation: MCTextField!
    @IBOutlet weak var textFieldType: MCTextField!
    @IBOutlet weak var textFieldLength: MCTextField!
    @IBOutlet weak var textFieldHealing: MCTextField!
    @IBOutlet weak var textFieldBoneType: MCTextField!
    @IBOutlet weak var textFieldPlacement: MCTextField!
    @IBOutlet weak var textFieldLotNumber: MCTextField!
    
    var completion:((TreatmentNotePopUp, String, String, String, String, String, String, String)->Void)?
    var viewController: UIViewController!
    var deletionBlock: ((TreatmentNotePopUp) -> Void)?
    
    @IBOutlet weak var buttonYes: UIButton!
    @IBOutlet weak var buttonNo: UIButton!
    
    func showInViewController(_ viewController: UIViewController!, withTreatmentNote note: TreatmentNotes?, deletionBlock: ((TreatmentNotePopUp) -> Void)?, completion : @escaping (_ popUpView: TreatmentNotePopUp, _ location: String, _ type: String, _ length: String, _ healing: String, _ boneType: String, _ placement: String, _ lotNumber: String) -> Void) {
        
        textFieldLocation.textFormat = .alphaNumeric
        textFieldType.textFormat = .alphaNumeric
        textFieldLength.textFormat = .alphaNumeric
        textFieldLotNumber.textFormat = .alphaNumeric
        
        self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        self.completion = completion
        self.viewController = viewController
        self.deletionBlock = deletionBlock
        
        if note == nil {
            textFieldLocation.text = ""
            textFieldType.text = ""
            textFieldLength.text = ""
            textFieldHealing.text = ""
            textFieldBoneType.text = ""
            textFieldPlacement.text = ""
            textFieldLotNumber.text = ""
            
            buttonNo.setTitle("CANCEL", for: UIControlState())
            buttonYes.setTitle("ADD", for: UIControlState())
        } else {
            textFieldLocation.text = note!.location
            textFieldType.text = note!.type
            textFieldLength.text = note!.length
            textFieldHealing.text = note!.healing
            textFieldBoneType.text = note!.boneType
            textFieldPlacement.text = note!.placement
            textFieldLotNumber.text = note!.lotNumber
            
            buttonNo.setTitle("DELETE", for: UIControlState())
            buttonYes.setTitle("UPDATE", for: UIControlState())
        }
        
        viewController!.view.addSubview(self)
        
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionOK(_ sender: AnyObject) {
        if allNil {
            self.close()
        } else {
            completion?(self, self.textFieldLocation.text!, self.textFieldType.text!, self.textFieldLength.text!, self.textFieldHealing.text!, self.textFieldBoneType.text!, self.textFieldPlacement.text!, self.textFieldLotNumber.text!)
        }
    }
    
    @IBAction func buttonCancelAction() {
        if deletionBlock == nil {
            self.close()
        } else {
            self.deletionBlock?(self)
        }
    }
    
    var allNil: Bool {
        get {
            return textFieldLocation.isEmpty && textFieldType.isEmpty && textFieldLength.isEmpty && textFieldHealing.isEmpty && textFieldBoneType.isEmpty && textFieldPlacement.isEmpty && textFieldLotNumber.isEmpty
        }
    }
    
    func close() {
        self.removeFromSuperview()
    }
}
