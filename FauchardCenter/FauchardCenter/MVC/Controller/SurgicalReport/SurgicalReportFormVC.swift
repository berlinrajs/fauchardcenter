//
//  SurgicalReportFormVC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 29/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalReportFormVC: SurgicalReportVC {
    
    @IBOutlet weak var labelRe: UILabel!
    @IBOutlet weak var labelDateSurgery: UILabel!
    @IBOutlet weak var labelSurgeryType: UILabel!
    @IBOutlet weak var labelFlapSurgery: UILabel!
    @IBOutlet weak var labelBoneGraftSurgery: UILabel!
    
    @IBOutlet weak var labelConnective: UILabel!
    @IBOutlet weak var labelExtraction: UILabel!
    @IBOutlet weak var labelBiopsy: UILabel!
    @IBOutlet weak var labelRoot: UILabel!
    @IBOutlet weak var labelCrown: UILabel!
    @IBOutlet weak var labelRidge: UILabel!
    
    @IBOutlet weak var labelPlanIncludes: UILabel!
    @IBOutlet weak var labelAnticipated1: UILabel!
    @IBOutlet weak var labelAnticipated2: UILabel!
    @IBOutlet weak var labelComments1: UILabel!
    @IBOutlet weak var labelComments2: UILabel!
    @IBOutlet weak var labelComments3: UILabel!
    @IBOutlet weak var labelComments4: UILabel!
    
    @IBOutlet weak var signatureView: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelDentistName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValues()
    }
    
    func loadValues() {
        labelRe.text = surgicalReport.reText
        labelDateSurgery.text = surgicalReport.dateOfSurgery
        labelSurgeryType.text = surgicalReport.typeOfSurgery
        labelFlapSurgery.text = surgicalReport.flapSurgery
        labelBoneGraftSurgery.text = surgicalReport.boneGraftSurgery
        labelConnective.text = surgicalReport.connectiveTissueGraft
        labelExtraction.text = surgicalReport.extraction
        labelBiopsy.text = surgicalReport.biopsy
        labelRoot.text = surgicalReport.rootAssumption
        labelCrown.text = surgicalReport.crownLengthening
        labelRidge.text = surgicalReport.ridgeAugmentation
        labelPlanIncludes.text = surgicalReport.planIncludes
        surgicalReport.anticipated.setTextForArrayOfLabels([labelAnticipated1, labelAnticipated2])
        surgicalReport.comments.setTextForArrayOfLabels([labelComments1, labelComments2, labelComments3, labelComments4])
        
        signatureView.image = surgicalReport.dentistSignature
        labelDate.text = patient.dateToday
        labelDentistName.text = patient.dentistName
    }
}
