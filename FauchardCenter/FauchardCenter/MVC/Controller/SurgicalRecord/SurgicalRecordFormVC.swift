//
//  SurgicalRecordStep7VC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 25/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalRecordFormVC: SurgicalRecordVC {

    @IBOutlet weak var radioTreatment: RadioButton!
    @IBOutlet weak var labelOtherTreatment: UILabel!
    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var labelDentistName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    @IBOutlet weak var labelLandmark1: UILabel!
    @IBOutlet weak var labelLandmark2: UILabel!
    @IBOutlet weak var labelLandmark3: UILabel!
    
    @IBOutlet var buttonsSurgicalSite: [UIButton]!
    @IBOutlet weak var radioAnterior: RadioButton!
    @IBOutlet weak var buttonAllergisNone: UIButton!
    @IBOutlet weak var labelAllergies: UILabel!
    @IBOutlet weak var radioEmotionalStatus: RadioButton!
    
    @IBOutlet weak var labelPreOPBP: UILabel!
    @IBOutlet weak var labelPostOPBP: UILabel!
    @IBOutlet weak var labelPreOPPulse: UILabel!
    @IBOutlet weak var labelPostOPPulse: UILabel!
    
    @IBOutlet var buttonsLocalAnesthetic: [UIButton]!
    @IBOutlet weak var labelLength1: UILabel!
    @IBOutlet weak var labelLength2: UILabel!
    @IBOutlet weak var labelLength3: UILabel!
    @IBOutlet weak var labelOther: UILabel!
    @IBOutlet weak var radioFlapDesign: RadioButton!
    
    @IBOutlet weak var radioHardTissue: RadioButton!
    @IBOutlet weak var radioGraftType: RadioButton!
    @IBOutlet weak var labelOtherGraft: UILabel!
    @IBOutlet weak var radioBiospy: RadioButton!
    @IBOutlet weak var buttonOccessous: UIButton!
    
    @IBOutlet weak var buttonAutoGenous: UIButton!
    @IBOutlet weak var labelAutoHarvested: UILabel!
    @IBOutlet weak var labelAutoVolume: UILabel!
    @IBOutlet weak var buttonAllograft: UIButton!
    @IBOutlet weak var labelAlloSource: UILabel!
    @IBOutlet weak var labelAlloBrand: UILabel!
    @IBOutlet weak var labelAlloVolume: UILabel!
    @IBOutlet weak var labelAlloMembrane: UILabel!
    @IBOutlet weak var radioResorbable: RadioButton!
    @IBOutlet weak var buttonConnextive: UIButton!
    @IBOutlet weak var radioHarvestedPlates: RadioButton!
    
    @IBOutlet weak var labelSuturesType: UILabel!
    @IBOutlet weak var labelSuturesNumber: UILabel!
    @IBOutlet weak var labelSuturesLocation: UILabel!
    
    @IBOutlet weak var radioPostOpIns: RadioButton!
    @IBOutlet weak var labelToWhom: UILabel!
    
    @IBOutlet weak var labelPreOPIns1: UILabel!
    @IBOutlet weak var labelPreOPIns2: UILabel!
    @IBOutlet weak var labelPreOPIns3: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValues()
    }
    
    func loadValues() {
        radioTreatment.setSelectedWithTag(surgicalRecord.treatmentTag == nil ? 0 : surgicalRecord.treatmentTag)
        labelOtherTreatment.text = surgicalRecord.otherTreatment
        labelPatientName.text = patient.fullName
        labelDentistName.text = patient.dentistName
        labelDate.text = patient.dateToday
        
        surgicalRecord.anatomicalLandmark.setTextForArrayOfLabels([labelLandmark1, labelLandmark2, labelLandmark3])
        
        for button in buttonsSurgicalSite {
            button.isSelected = surgicalRecord.surgicalSiteTags.contains(button.tag)
        }
        
        radioAnterior.setSelectedWithTag(surgicalRecord.isAnterior == nil ? 0 : surgicalRecord.isAnterior)
        buttonAllergisNone.isSelected = surgicalRecord.haveAllergies == 2
        labelAllergies.text = surgicalRecord.allergies
        radioEmotionalStatus.setSelectedWithTag(surgicalRecord.emotionalStatusTag == nil ? 0 : surgicalRecord.emotionalStatusTag)
        labelPreOPBP.text = surgicalRecord.bPpreOpHigh + "/" + surgicalRecord.bPpreOpLow
        labelPostOPBP.text = surgicalRecord.bPpostOpHigh + "/" + surgicalRecord.bPpostOpLow
        labelPreOPPulse.text = surgicalRecord.pulsePreOp
        labelPostOPPulse.text = surgicalRecord.pulsePostOp
        
        for button in buttonsLocalAnesthetic {
            button.isSelected = surgicalRecord.localAnestheticTags.contains(button.tag)
        }
        
        labelLength1.text = surgicalRecord.length1
        labelLength2.text = surgicalRecord.length2
        labelLength3.text = surgicalRecord.length3
        labelOther.text = surgicalRecord.incisionsOther
        radioFlapDesign.setSelectedWithTag(surgicalRecord.flapThickenssTag == nil ? 0 : surgicalRecord.flapThickenssTag)
        
        radioHardTissue.setSelectedWithTag(surgicalRecord.isHardTissue == nil ? 0 : surgicalRecord.isHardTissue)
        radioGraftType.setSelectedWithTag(surgicalRecord.graftType == nil ? 0 : surgicalRecord.graftType)
        labelOtherGraft.text = surgicalRecord.OtherGraftType
        radioBiospy.setSelectedWithTag(surgicalRecord.isBiopsy == nil ? 0 : surgicalRecord.isBiopsy)
        buttonOccessous.isSelected = surgicalRecord.osseousWall == 1
        buttonAutoGenous.isSelected = surgicalRecord.isAutogenous == nil ? false : surgicalRecord.isAutogenous
        labelAutoHarvested.text = surgicalRecord.autogenousHarvestedFrom
        labelAutoVolume.text = surgicalRecord.autogenousVolume
        buttonAllograft.isSelected = surgicalRecord.isAllograft == nil ? false : surgicalRecord.isAllograft
        labelAlloSource.text = surgicalRecord.allograftSource
        labelAlloBrand.text = surgicalRecord.allograftBrand
        labelAlloVolume.text = surgicalRecord.allograftVolume
        labelAlloMembrane.text = surgicalRecord.membrane
        radioResorbable.setSelectedWithTag(surgicalRecord.isResorbable == nil ? 0 : surgicalRecord.isResorbable)
        buttonConnextive.isSelected = surgicalRecord.isConnectiveTissue
        radioHarvestedPlates.setSelectedWithTag(surgicalRecord.isHarvestedFromPlate == nil ? 0 : surgicalRecord.isHarvestedFromPlate)
        labelSuturesType.text = surgicalRecord.suturesType
        labelSuturesNumber.text = surgicalRecord.suturesNumber
        labelSuturesLocation.text = surgicalRecord.suturesLocation
        radioPostOpIns.setSelectedWithTag(surgicalRecord.isGivenPostOpIns == nil ? 0 : surgicalRecord.isGivenPostOpIns)
        labelToWhom.text = surgicalRecord.givenToWhom
        
        surgicalRecord.preOpIns.setTextForArrayOfLabels([labelPreOPIns1, labelPreOPIns2, labelPreOPIns3])
    }
}
