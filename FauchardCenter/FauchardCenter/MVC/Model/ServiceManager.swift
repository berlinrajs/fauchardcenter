//
//  ServiceManager.swift
//   Angell Family Dentistry
//
//  Created by Bose on 02/02/16.
//  Copyright © 2016  Angell Family Dentistry. All rights reserved.
//

import UIKit

class ServiceManager: NSObject {
    
    class func fetchDataFromService(_ baseUrlString: String, serviceName: String, parameters : [String : String]?, success:@escaping (_ result : AnyObject) -> Void, failure :@escaping (_ error : NSError) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: baseUrlString))
        
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.post(serviceName, parameters: parameters, progress: { (progress) in
            }, success: { (task, result) in
                success(result!as AnyObject)
        }) { (task, error) in
            failure(error as NSError)
        }
    }
    
    class func fetchDataFromServiceCheckIn(_ serviceName: String, parameters : [String : String]?, success:@escaping (_ result : AnyObject) -> Void, failure :@escaping (_ error : NSError) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: "https://alpha.mncell.com/mconsent/"))
        
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.post(serviceName, parameters: parameters, progress: { (progress) in
            }, success: { (task, result) in
                success(result as AnyObject)
        }) { (task, error) in
            failure(error as NSError)
        }
    }
    class func loginWithUsername(_ userName: String, password: String, completion: @escaping (_ success: Bool, _ error: NSError?) -> Void) {
        ServiceManager.fetchDataFromService("https://mncell.com/mclogin/", serviceName: "apploginapi.php?", parameters: ["appkey": kAppKey, "username": userName, "password": password], success: { (result) in
            if (result["posts"]! as! NSDictionary)["status"] as! String == "success" {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"]! as! NSDictionary)["message"] as! String]))
            }
        }) { (error) in
            completion(false, nil)
        }
    }
    class func ChekInFormStatus(_ patientName: String, patientPurpose: String, completion: @escaping (_ success: Bool, _ error: NSError?) -> Void) {
        //"mcdistinctivelaser"
        ServiceManager.fetchDataFromServiceCheckIn("savepatients_info.php?", parameters: ["patientkey": kAppKey, "patientname": patientName, "patientpurpose": patientPurpose], success: { (result) in
            if (result["posts"] as! String == "success") {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"]! as! NSDictionary)["message"] as! String]))
            }
        }) { (error) in
            completion(false, nil)
        }
    }
    
    class func postReview(_ name: String, comment: String, rating: CGFloat, phoneNumber: String, allowMessage: Bool, email: String, anonymous: Bool, completion: @escaping (_ success: Bool, _ error: NSError?) -> Void) {
        ServiceManager.fetchDataFromService("https://alpha.mncell.com/review/", serviceName: "app_review.php?", parameters: ["patient_appkey": kAppKey, "patient_name": name, "patient_review_comment": comment, "patient_rating": "\(rating)", "patient_phone_number": phoneNumber, "sms_allowed": allowMessage == true ? "1" : "0", "patient_email": email, "user_anonymous": anonymous == true ? "1" : "0"], success: { (result) in
            if result["posts"] != nil && (result["posts"] as! NSDictionary)["status"] as! String == "success" {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"] as! NSDictionary)["message"] as! String]))
            }
        }) { (error) in
            completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong, Please Check your internet connection and try again"]))
        }
    }
}
