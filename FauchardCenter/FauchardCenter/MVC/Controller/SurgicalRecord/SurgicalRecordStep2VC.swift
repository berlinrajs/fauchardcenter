//
//  SurgicalRecordStep2VC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 25/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalRecordStep2VC: SurgicalRecordVC {

    @IBOutlet weak var dropDownEmotional: BRDropDown!
    @IBOutlet weak var radioAllergies: RadioButton!
    @IBOutlet weak var textFieldPreHigh: MCTextField!
    @IBOutlet weak var textFieldPreLow: MCTextField!
    
    @IBOutlet weak var textFieldPostHigh: MCTextField!
    @IBOutlet weak var textFieldPostLow: MCTextField!
    
    var otherAllergies: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textFieldPreHigh.setNumberFormatWithCount(3, limit: 0)
        textFieldPreLow.setNumberFormatWithCount(3, limit: 0)
        textFieldPostHigh.setNumberFormatWithCount(3, limit: 0)
        textFieldPostLow.setNumberFormatWithCount(3, limit: 0)
        
        dropDownEmotional.items = ["Calm", "Cooperative", "Nervous", "Agitated"]
        dropDownEmotional.placeholder = "-- SELECT --"
        
        loadValues()
    }
    
    func loadValues() {
        dropDownEmotional.selectedIndex = self.surgicalRecord.emotionalStatusTag == nil ? 0 : self.surgicalRecord.emotionalStatusTag
        radioAllergies.setSelectedWithTag(surgicalRecord.haveAllergies == nil ? 0 : surgicalRecord.haveAllergies)
        
        textFieldPreHigh.text = surgicalRecord.bPpreOpHigh
        textFieldPreLow.text = surgicalRecord.bPpreOpLow
        textFieldPostHigh.text = surgicalRecord.bPpostOpHigh
        textFieldPostLow.text = surgicalRecord.bPpostOpLow
        
        self.otherAllergies = surgicalRecord.allergies
    }
    
    func saveValues() {
        self.view.endEditing(true)
        
        self.surgicalRecord.emotionalStatusTag = dropDownEmotional.selectedIndex
        surgicalRecord.haveAllergies = radioAllergies.selected == nil ? 0 : radioAllergies.selected.tag
        
        surgicalRecord.bPpreOpHigh = textFieldPreHigh.text!
        surgicalRecord.bPpreOpLow = textFieldPreLow.text!
        surgicalRecord.bPpostOpHigh = textFieldPostHigh.text!
        surgicalRecord.bPpostOpLow = textFieldPostLow.text!
        
        surgicalRecord.allergies = self.otherAllergies
    }
    
    @IBAction func buttonAllergiesAction () {
        if radioAllergies.isSelected {
            PopupTextView.popUpView().showWithPlaceHolder("PLEASE LIST HERE", completion: { (popUpView, textView) in
                popUpView.close()
                if textView.isEmpty {
                    self.otherAllergies = ""
                    self.radioAllergies.isSelected = false
                } else {
                    self.otherAllergies = textView.textValue
                }
            })
        } else {
            self.otherAllergies = ""
        }
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonNextAction() {
        saveValues()
        
        let step3VC = self.storyboard?.instantiateViewController(withIdentifier: "kSurgicalRecordStep3VC") as! SurgicalRecordStep3VC
        step3VC.patient = self.patient
        self.navigationController?.pushViewController(step3VC, animated: true)
    }
}
