//
//  NewPatient17ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/28/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient17ViewController: MCViewController {

    @IBOutlet weak var textfieldReferringDentist : MCTextField!
    @IBOutlet weak var textfieldOtherDentist : MCTextField!
    @IBOutlet weak var textfieldSpouse : MCTextField!
    @IBOutlet weak var textfieldChildren : MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        textfieldReferringDentist.setSavedText(patient.newPatient.referringDentist)
        textfieldOtherDentist.setSavedText(patient.newPatient.otherDentist)
        textfieldSpouse.setSavedText(patient.newPatient.spouse)
        textfieldChildren.setSavedText(patient.newPatient.children)
        
    }
    
    func saveValue()  {
        patient.newPatient.referringDentist = textfieldReferringDentist.getText()
        patient.newPatient.otherDentist = textfieldOtherDentist.getText()
        patient.newPatient.spouse = textfieldSpouse.getText()
        patient.newPatient.children = textfieldChildren.getText()
        
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
            self.view.endEditing(true)
            saveValue()
            let new = newPatientStoryBoard.instantiateViewControllerWithIdentifier("NewPatient18VC") as! NewPatient18ViewController
            new.patient = self.patient
            self.navigationController?.pushViewController(new, animated: true)
    }
    
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }


}
