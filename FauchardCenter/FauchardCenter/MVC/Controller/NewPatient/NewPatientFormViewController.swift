//
//  NewPatientFormViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/28/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatientFormViewController: MCViewController {

    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelFirstName : UILabel!
    @IBOutlet weak var labelLastName : UILabel!
    @IBOutlet weak var labelMiddleInitial : UILabel!
    @IBOutlet weak var radioTitle : RadioButton!
    @IBOutlet weak var radioMaritalStatus : RadioButton!
    @IBOutlet weak var radioLegalName : RadioButton!
    @IBOutlet weak var labelLegalName : UILabel!
    @IBOutlet weak var labelFormerName : UILabel!
    @IBOutlet weak var labelBirthdate : UILabel!
    @IBOutlet weak var labelAge : UILabel!
    @IBOutlet weak var radioGender : RadioButton!
    @IBOutlet weak var labelAddress : UILabel!
    @IBOutlet weak var labelSSN : UILabel!
    @IBOutlet weak var labelHomePhone : UILabel!
    @IBOutlet weak var labelPOBox : UILabel!
    @IBOutlet weak var labelCity : UILabel!
    @IBOutlet weak var labelState : UILabel!
    @IBOutlet weak var labelZipcode : UILabel!
    @IBOutlet weak var labelOccupation : UILabel!
    @IBOutlet weak var labelEmployer : UILabel!
    @IBOutlet weak var labelEmployerPhone : UILabel!
    @IBOutlet weak var labelReferenceDoctor : UILabel!
    @IBOutlet weak var labelReferenceOther : UILabel!
    @IBOutlet weak var radioHaveDentalInsurance : RadioButton!
    @IBOutlet weak var radioReference : RadioButton!
    @IBOutlet weak var labelResponsibleName : UILabel!
    @IBOutlet weak var labelResponsibleBirthdate : UILabel!
    @IBOutlet weak var labelResponsibleAddress : UILabel!
    @IBOutlet weak var labelResponsibleHomePhone : UILabel!
    @IBOutlet weak var labelResponsibleOccupation : UILabel!
    @IBOutlet weak var labelResponsibleEmployer : UILabel!
    @IBOutlet weak var labelResponsibleEmployerAddress : UILabel!
    @IBOutlet weak var labelResponsibleEmployerPhone : UILabel!
    @IBOutlet weak var radioInsurance : RadioButton!
    @IBOutlet weak var radioRelationship : RadioButton!
    @IBOutlet weak var labelResponsibleRelationOther : UILabel!
    @IBOutlet weak var labelPrimaryInsurance : UILabel!
    @IBOutlet weak var labelPrimarySubscriber : UILabel!
    @IBOutlet weak var labelPrimaryDOB : UILabel!
    @IBOutlet weak var labelPrimarySSN : UILabel!
    @IBOutlet weak var labelPrimaryGroup : UILabel!
    @IBOutlet weak var labelPrimaryPhoneNumber : UILabel!
    @IBOutlet weak var labelSecondaryInsurance : UILabel!
    @IBOutlet weak var labelSecondarySubscriber : UILabel!
    @IBOutlet weak var labelSecondaryDOB : UILabel!
    @IBOutlet weak var labelSecondarySSN : UILabel!
    
    
    @IBOutlet weak var labelEmergencyName : UILabel!
    @IBOutlet weak var labelEmergencyRelationship : UILabel!
    @IBOutlet weak var labelEmergencyHomePhone : UILabel!
    @IBOutlet weak var labelEmergencyWorkPhone : UILabel!
    @IBOutlet weak var signaturePatient1 : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var radioBirthControl : RadioButton!
    @IBOutlet weak var radioPregnant : RadioButton!
    @IBOutlet weak var labelNumberOfWeeks : UILabel!
    @IBOutlet weak var radioNursing : RadioButton!
    @IBOutlet weak var radioSmoking : RadioButton!
    @IBOutlet weak var labelHowMuch : UILabel!
    @IBOutlet weak var labelHowLong : UILabel!
    @IBOutlet weak var labelHeight : UILabel!
    @IBOutlet weak var labelWeight : UILabel!
    @IBOutlet weak var labelCancerType : UILabel!
    @IBOutlet      var arrayButtons1 : [RadioButton]!
    @IBOutlet      var arrayButtons2 : [RadioButton]!
    @IBOutlet      var arrayButtons3 : [RadioButton]!
    @IBOutlet      var arrayButtons4 : [RadioButton]!
    @IBOutlet      var arrayButtonsAllergies : [RadioButton]!
    @IBOutlet      var labelOtherAllergy : [UILabel]!
    @IBOutlet      var labelMedications : [UILabel]!
   


    @IBOutlet      var labelExtraProblems : [UILabel]!
    @IBOutlet      var arrayButtons6 : [RadioButton]!
    @IBOutlet      var arrayButtons7 : [RadioButton]!
     @IBOutlet      var labelMouthInjury : [UILabel]!
    
    @IBOutlet      var labelPersonalHistory : [UILabel]!
    @IBOutlet      var labelMedicalHistory : [UILabel]!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var signaturePatient2 : UIImageView!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelPhysicianName : UILabel!
    @IBOutlet weak var labelPhysicianPhone : UILabel!
    @IBOutlet weak var labelPharmacy : UILabel!
    @IBOutlet weak var labelPharmacyPhone : UILabel!

    
    
    @IBOutlet  var labelOtherInformation : [UILabel]!
    @IBOutlet      var labelLimitations : [UILabel]!
    @IBOutlet weak var signaturePatient3 : UIImageView!
    @IBOutlet weak var labelDate3 : UILabel!
    @IBOutlet weak var labelDate4 : UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadPage1()
        loadPage2()
        loadPage3()
        loadPage4()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadPage1()  {
        labelDate.text = patient.dateToday
        labelFirstName.text = patient.firstName
        labelLastName.text = patient.lastName
        labelMiddleInitial.text = patient.initial
        radioTitle.setSelectedWithTag(patient.newPatient.title)
        radioMaritalStatus.setSelectedWithTag(patient.newPatient.maritalStatus)
        radioLegalName.setSelectedWithTag(patient.newPatient.legalTag)
        labelLegalName.text = patient.newPatient.legalName
        labelFormerName.text = patient.newPatient.formerName
        labelBirthdate.text = patient.dateOfBirth
        labelAge.text = "\(patient.patientAge!)"
        radioGender.setSelectedWithTag(patient.newPatient.gender)
        labelAddress.text = patient.newPatient.address
        labelSSN.text = patient.newPatient.socialSecurity.socialSecurityNumber
        labelHomePhone.text = patient.newPatient.phoneNumber
        labelPOBox.text = patient.newPatient.postBox
        labelCity.text = patient.newPatient.city
        labelState.text = patient.newPatient.state
        labelZipcode.text = patient.newPatient.zipcode
        labelOccupation.text = patient.newPatient.occupation
        labelEmployer.text = patient.newPatient.employerName
        labelEmployerPhone.text = patient.newPatient.employerPhone
        labelReferenceDoctor.text = patient.newPatient.referalDoctorName
        labelReferenceOther .text = patient.newPatient.referalOther
        radioReference.setSelectedWithTag(patient.newPatient.reference)
        radioHaveDentalInsurance.setSelectedWithTag(patient.newPatient.dentalInsuranceTag)
        labelResponsibleName.text = patient.newPatient.responsibleName
        labelResponsibleBirthdate.text = patient.newPatient.responsibleBirthdate
        labelResponsibleAddress.text = patient.newPatient.responsibleAddress
        labelResponsibleHomePhone.text = patient.newPatient.responsiblePhoneNumber
        labelResponsibleOccupation.text = patient.newPatient.responsibleOccupation
        labelResponsibleEmployer.text = patient.newPatient.responsibleEmployerName
        labelResponsibleEmployerAddress.text = patient.newPatient.responsibleEmployerAddress
        labelResponsibleEmployerPhone.text = patient.newPatient.responsibleEmployerPhone
        radioInsurance.setSelectedWithTag(patient.newPatient.radioHasInsurance)
        radioRelationship.setSelectedWithTag(patient.newPatient.responsibleRelationship)
        labelResponsibleRelationOther.text = patient.newPatient.responsibleRelationshipOther
        labelPrimaryInsurance.text = patient.newPatient.primaryInsurance.insuranceName
        labelPrimarySubscriber.text = patient.newPatient.primaryInsurance.subscriberName
        labelPrimaryDOB.text = patient.newPatient.primaryInsurance.DateOfBirth
        labelPrimarySSN.text = patient.newPatient.primaryInsurance.socialSecurity.socialSecurityNumber
        labelPrimaryGroup.text = patient.newPatient.primaryInsurance.group
        labelPrimaryPhoneNumber.text = patient.newPatient.primaryInsurance.phoneNumber
        labelSecondaryInsurance.text = patient.newPatient.secondaryInsurance.insuranceName
        labelSecondarySubscriber.text = patient.newPatient.secondaryInsurance.subscriberName
        labelSecondaryDOB.text = patient.newPatient.secondaryInsurance.DateOfBirth
        labelSecondarySSN.text = patient.newPatient.secondaryInsurance.socialSecurity.socialSecurityNumber
    }

    func loadPage2()  {
        labelEmergencyName.text = patient.newPatient.emergencyName
        labelEmergencyRelationship.text = patient.newPatient.emergencyRelationship
        labelEmergencyHomePhone.text = patient.newPatient.emergencyHomePhone
        labelEmergencyWorkPhone.text = patient.newPatient.emergencyWorkPhone
        signaturePatient1.image = patient.newPatient.patientSignature1
        labelDate1.text = patient.dateToday
        radioBirthControl.setSelectedWithTag(patient.newPatient.birthControlTag)
        radioPregnant.setSelectedWithTag(patient.newPatient.pregnantTag)
        labelNumberOfWeeks.text = patient.newPatient.pregnantWeeks
        radioNursing.setSelectedWithTag(patient.newPatient.nursingTag)
        radioSmoking.setSelectedWithTag(patient.newPatient.smokeTag)
        labelHowMuch.text = patient.newPatient.howMuch
        labelHowLong.text = patient.newPatient.howLong
        labelHeight.text = patient.newPatient.height
        labelWeight.text = patient.newPatient.weight
        labelCancerType.text = patient.newPatient.medicalQuestions[2][10].answer
        for btn in arrayButtons1{
            btn.isSelected = patient.newPatient.medicalQuestions[0][btn.tag].selectedOption == true
        }
        for btn in arrayButtons2{
            btn.isSelected = patient.newPatient.medicalQuestions[1][btn.tag].selectedOption == true
        }
        for btn in arrayButtons3{
            btn.isSelected = patient.newPatient.medicalQuestions[2][btn.tag].selectedOption == true
        }
        for btn in arrayButtons4{
            btn.isSelected = patient.newPatient.medicalQuestions[3][btn.tag].selectedOption == true
        }

        for btn in arrayButtonsAllergies{
            btn.isSelected = patient.newPatient.allergies[btn.tag].selectedOption == true
        }
        patient.newPatient.medications.setTextForArrayOfLabels(labelMedications.sorted(by: { (label1, label2) -> Bool in
            return label1.tag < label2.tag
        }))
        
        if patient.newPatient.allergies[9].answer != nil
        {
            patient.newPatient.allergies[9].answer.setTextForArrayOfLabels(labelOtherAllergy.sorted(by: { (label1, label2) -> Bool in
                return label1.tag < label2.tag
            }))
        }
        
        
    }

    func loadPage3 ()  {
        
        patient.newPatient.extraProblems.setTextForArrayOfLabels(labelExtraProblems.sorted(by: { (label1, label2) -> Bool in
            return label1.tag < label2.tag
        }))
        
        for btn in arrayButtons6{
            btn.isSelected = patient.newPatient.medicalHistory[0][btn.tag].selectedOption == true
        }
        for btn in arrayButtons7{
            btn.isSelected = patient.newPatient.medicalHistory[1][btn.tag].selectedOption == true
        }
        

        
        
        patient.newPatient.surgeryProcedures.setTextForArrayOfLabels(labelMedicalHistory.sorted(by: { (label1, label2) -> Bool in
            return label1.tag < label2.tag
        }))
        patient.newPatient.mouthInjury.setTextForArrayOfLabels(labelMouthInjury.sorted(by: { (label1, label2) -> Bool in
            return label1.tag < label2.tag
        }))
        
        labelPatientName.text = patient.fullName
        signaturePatient2.image = patient.newPatient.patientSignature2
        labelDate2.text = patient.dateToday
        labelPhysicianName.text = patient.newPatient.physicianName
        labelPhysicianPhone.text = patient.newPatient.physicianPhone
        labelPharmacy.text = patient.newPatient.pharmacy
        labelPharmacyPhone.text = patient.newPatient.pharmacyPhone
    }
    
    func loadPage4()  {
       
        
        
        patient.newPatient.limitations.setTextForArrayOfLabels(labelLimitations.sorted(by: { (label1, label2) -> Bool in
            return label1.tag < label2.tag
        }))
        patient.newPatient.otherInformations.setTextForArrayOfLabels(labelOtherInformation.sorted(by: { (label1, label2) -> Bool in
            return label1.tag < label2.tag
        }))
        
        signaturePatient3.image = patient.newPatient.patientSignature4
        labelDate3.text = patient.dateToday
        
        labelDate4.text = patient.dateToday
    }

}
