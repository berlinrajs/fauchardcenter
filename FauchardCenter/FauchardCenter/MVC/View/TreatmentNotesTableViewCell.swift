//
//  TreatmentNotesTableViewCell.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 30/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class TreatmentNotesTableViewCell: UITableViewCell {

    @IBOutlet weak var labelLocation: UILabel!
    @IBOutlet weak var labelType: UILabel!
    @IBOutlet weak var labelLength: UILabel!
    @IBOutlet weak var labelHealing: UILabel!
    @IBOutlet weak var labelBoneType: UILabel!
    @IBOutlet weak var labelPlacement: UILabel!
    @IBOutlet weak var labelLotNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configWithNotes(_ treatmentNote: TreatmentNotes) {
        labelLocation.text = treatmentNote.location
        labelType.text = treatmentNote.type
        labelLength.text = treatmentNote.length
        labelHealing.text = treatmentNote.healing
        labelBoneType.text = treatmentNote.boneType
        labelPlacement.text = treatmentNote.placement
        labelLotNumber.text = treatmentNote.lotNumber
    }
}

class TreatmentNotes: NSObject {
    var location: String!
    var type: String!
    var length: String!
    var healing: String!
    var boneType: String!
    var placement: String!
    var lotNumber: String!
}
