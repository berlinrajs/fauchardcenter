//
//  NewPatient5ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/24/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient5ViewController: MCViewController {

    @IBOutlet weak var dropDownRelationship : BRDropDown!
    @IBOutlet weak var textfieldName : MCTextField!
    @IBOutlet weak var textfieldAddress : MCTextField!
    @IBOutlet weak var textfieldBirthDate : MCTextField!
    @IBOutlet weak var textfieldPhoneNumber : MCTextField!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    func loadValue()  {
        dropDownRelationship.placeholder = "-- SELECT --"
        dropDownRelationship.items = ["SELF","SPOUSE","CHILD","OTHER"]
        dropDownRelationship.delegate = self
        textfieldBirthDate.textFormat = .dateIn1980
        textfieldPhoneNumber.textFormat = .phone
        dropDownRelationship.selectedIndex = patient.newPatient.responsibleRelationship
        textfieldName.setSavedText(patient.newPatient.responsibleName)
        textfieldAddress.setSavedText(patient.newPatient.responsibleAddress)
        textfieldBirthDate.setSavedText(patient.newPatient.responsibleBirthdate)
        textfieldPhoneNumber.setSavedText(patient.newPatient.responsiblePhoneNumber)
       

    }
    
    func saveValue()  {
        patient.newPatient.responsibleName = textfieldName.getText()
        patient.newPatient.responsibleRelationship = dropDownRelationship.selectedOption == nil ? 0 : dropDownRelationship.selectedIndex
        patient.newPatient.responsibleAddress = textfieldAddress.getText()
        patient.newPatient.responsibleBirthdate = textfieldBirthDate.getText()
        patient.newPatient.responsiblePhoneNumber = textfieldPhoneNumber.getText()
        

        
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        self.view.endEditing(true)
        if  dropDownRelationship.selectedOption == nil || textfieldName.isEmpty{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if !textfieldPhoneNumber.isEmpty && !textfieldPhoneNumber.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID HOME PHONE NUMBER")
        }else{
            saveValue()
            let new = newPatientStoryBoard.instantiateViewController(withIdentifier: "NewPatient6VC") as! NewPatient6ViewController
            new.isSelfSelected = dropDownRelationship.selectedIndex == 1
            new.patient = self.patient
            self.navigationController?.pushViewController(new, animated: true)
        }
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
}

extension NewPatient5ViewController : BRDropDownDelegate{
    func dropDown(_ dropDown: BRDropDown, selectedAtIndex index: Int, selectedOption option: String?) {
        if index == 1 {
            textfieldName.setSavedText(patient.fullName)
            textfieldAddress.setSavedText(patient.newPatient.address)
            textfieldPhoneNumber.setSavedText(patient.newPatient.phoneNumber)
            textfieldBirthDate.setSavedText(patient.dateOfBirth)
        }else{
            textfieldName.text = ""
            textfieldAddress.text = ""
            textfieldPhoneNumber.text = ""
            textfieldBirthDate.text = ""
        }
        
        if index == 4{
            PopupTextField.popUpView().showInViewController(self, WithTitle: "", placeHolder: "PLEASE SPECIFY", textFormat: TextFormat.default, completion: { (popUpView, textField) in
                if textField.isEmpty{
                    self.patient.newPatient.responsibleRelationshipOther = "N/A"
                }else{
                    self.patient.newPatient.responsibleRelationshipOther = textField.text!
                }
                popUpView.removeFromSuperview()
            })
        }else{
            self.patient.newPatient.responsibleRelationshipOther = "N/A"

        }
    }
}
