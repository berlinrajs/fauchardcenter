//
//  SurgicalRecordStep6VC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 25/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalRecordStep6VC: SurgicalRecordVC {

    @IBOutlet weak var textFieldType: MCTextField!
    @IBOutlet weak var textFieldnumber: MCTextField!
    @IBOutlet weak var textFieldLocation: MCTextField!
    
    @IBOutlet weak var radioGivenIns: RadioButton!
    @IBOutlet weak var textViewPrescriptions: MCTextView!
    var towhom: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textFieldLocation.textFormat = .alphaNumeric
        textFieldnumber.textFormat = .alphaNumeric
        
        loadValues()
    }
    
    func loadValues() {
        textFieldType.text = surgicalRecord.suturesType
        textFieldnumber.text = surgicalRecord.suturesNumber
        textFieldLocation.text = surgicalRecord.suturesLocation
        radioGivenIns.setSelectedWithTag(surgicalRecord.isGivenPostOpIns == nil ? 0 : surgicalRecord.isGivenPostOpIns)
        self.towhom = surgicalRecord.givenToWhom
        textViewPrescriptions.textValue = surgicalRecord.preOpIns
    }
    
    func saveValues() {
        self.view.endEditing(true)
        
        surgicalRecord.suturesType = textFieldType.text!
        surgicalRecord.suturesNumber = textFieldnumber.text!
        surgicalRecord.suturesLocation = textFieldLocation.text!
        surgicalRecord.isGivenPostOpIns = radioGivenIns.selected == nil ? 0 : radioGivenIns.selected.tag
        surgicalRecord.givenToWhom = self.towhom
        surgicalRecord.preOpIns = textViewPrescriptions.textValue!
    }
    
    @IBAction func radioGivenAction(_ sender: UIButton) {
        if radioGivenIns.isSelected {
            PopupTextField.popUpView().showInViewController(self, WithTitle: "GIVEN TO WHOM?", placeHolder: "NAME *", textFormat: TextFormat.default) { (popUpView, textField) in
                popUpView.close()
                if !textField.isEmpty {
                    self.towhom = textField.text!
                } else {
                    self.towhom = ""
                    self.radioGivenIns.isSelected = false
                }
            }
        } else {
            self.towhom = ""
        }
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonNextAction() {
        saveValues()
        let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kSurgicalRecordFormVC") as! SurgicalRecordFormVC
        formVC.patient = self.patient
        self.navigationController?.pushViewController(formVC, animated: true)
    }
}
