//
//  NewPatient15ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/25/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient15ViewController: MCViewController {

    @IBOutlet weak var textviewMedication : MCTextView!
    @IBOutlet weak var textviewExtraProblems : MCTextView!
    @IBOutlet weak var textviewConcern : MCTextView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        textviewMedication.placeholder = "PLEASE TYPE HERE"
        textviewMedication.text = patient.newPatient.medications == "N/A" ? "PLEASE TYPE HERE" : patient.newPatient.medications
        textviewMedication.textColor =  patient.newPatient.medications == "N/A" ? UIColor.lightGray : UIColor.black
        
        textviewExtraProblems.placeholder = "PLEASE TYPE HERE"
        textviewExtraProblems.text = patient.newPatient.extraProblems == "N/A" ? "PLEASE TYPE HERE" : patient.newPatient.extraProblems
        textviewExtraProblems.textColor =  patient.newPatient.extraProblems == "N/A" ? UIColor.lightGray : UIColor.black
        
        textviewConcern.placeholder = "PLEASE TYPE HERE"
        textviewConcern.text = patient.newPatient.concern == "N/A" ? "PLEASE TYPE HERE" : patient.newPatient.concern
        textviewConcern.textColor =  patient.newPatient.concern == "N/A" ? UIColor.lightGray : UIColor.black

       
    }
    
    func saveValue()  {
        patient.newPatient.medications = textviewMedication.isEmpty ? "N/A" : textviewMedication.text!
        patient.newPatient.extraProblems = textviewExtraProblems.isEmpty ? "N/A" : textviewExtraProblems.text!
        patient.newPatient.concern = textviewConcern.isEmpty ? "N/A" : textviewConcern.text!
        
        
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        self.view.endEditing(true)
            saveValue()
        let new = newPatientStoryBoard.instantiateViewController(withIdentifier: "NewPatient16VC") as! NewPatient16ViewController
        new.patient = self.patient
        self.navigationController?.pushViewController(new, animated: true)
        

    }
    
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }


}
