//
//  SurgicalRecordWisdomVC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 28/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalRecordWisdomVC: MCViewController {

    var surgicalRecordWisdom: SurgicalRecordWisdom! {
        get {
            return patient.surgicalRecordWisdom
        } set {
            patient.surgicalRecordWisdom = newValue
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
