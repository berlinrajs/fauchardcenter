//
//  MedicalHistory1ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/23/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistory1ViewController: MCViewController {
    
    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var buttonVerified: UIButton!
    @IBOutlet var labelHeading : UILabel!
    var selectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.labelHeading.text = self.selectedIndex == 3 ? "" : "Have you ever had any of the following: *"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if buttonVerified.isSelected == false {
            self.showAlert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        } else {
            if selectedIndex == 3 {
                let medical = mainStoryBoard.instantiateViewController(withIdentifier: "Medical2VC") as! MedicalHistory2ViewController
                medical.patient = self.patient
                self.navigationController?.pushViewController(medical, animated: true)
            } else {
                let medical = mainStoryBoard.instantiateViewController(withIdentifier: "Medical1VC") as! MedicalHistory1ViewController
                medical.patient = self.patient
                medical.selectedIndex = self.selectedIndex + 1
                self.navigationController?.pushViewController(medical, animated: true)
            }
        }
    }
}

extension MedicalHistory1ViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.medical.medicalQuestions[selectedIndex].count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return selectedIndex == 3 ? 60 : 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PatientInfoCell
        cell.delegate = self
        cell.tag = indexPath.row
        cell.configCell(self.patient.medical.medicalQuestions[selectedIndex][indexPath.row])
        
        return cell
    }
}

extension MedicalHistory1ViewController : PatientInfoCellDelegate{
    
    func radioButtonTappedForCell(_ cell: PatientInfoCell) {
        if selectedIndex == 2{
        PopupTextField.popUpView().showInViewController(self, WithTitle: "", placeHolder: "PLEASE SPECIFY", textFormat: TextFormat.default) { (popUpView, textField) in
            if !textField.isEmpty{
                self.patient.medical.medicalQuestions[self.selectedIndex][cell.tag].answer = textField.text!
            }else{
                cell.radioButtonYes.isSelected = false
                self.patient.medical.medicalQuestions[self.selectedIndex][cell.tag].selectedOption = false
            }
            popUpView.removeFromSuperview()
        }
        }else{
            PopupTextView.sharedInstance.showWithPlaceHolder("PLEASE EXPLAIN", completion: { (popUpView, textView) in
                if !textView.isEmpty{
                    self.patient.medical.medicalQuestions[self.selectedIndex][cell.tag].answer = textView.text
                }else{
                    cell.radioButtonYes.isSelected = false
                    self.patient.medical.medicalQuestions[self.selectedIndex][cell.tag].selectedOption = false
                }
                popUpView.removeFromSuperview()

            })
        }
    }
    
}
