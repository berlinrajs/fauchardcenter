//
//  ImplantDebridementConsentVC.swift
//  FauchardCenter
//
//  Created by Manjusha Chembra on 7/31/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import Foundation

class ImplantDebridementConsentVC: MCViewController {
    
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelTitle : UILabel!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var labelDate2 : DateLabel!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var signatureWitness : SignatureView!
    @IBOutlet weak var textfieldWitnessName : MCTextField!
    @IBOutlet weak var labelImplantNumber : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        if patient.is18YearsOld{
            labelTitle.text = "Patient Signature"
            labelPatientName.text = patient.fullName
        }else{
            labelTitle.text = "Guardian, or Authorized Representative Signature"
            labelPatientName.text = ""
        }
        let form : Forms = patient.selectedForms.first!
        labelImplantNumber.text = labelImplantNumber.text?.replacingOccurrences(of: "kImplantNumber", with: form.toothNumbers)
        
    }
    
    
    func saveValues()
    {
        patient.consentWitnessName = textfieldWitnessName.text!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        self.view.endEditing(true)
        if !signaturePatient.isSigned() || !signatureWitness.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped || !labelDate2.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValues()
            let form = consentStoryBoard.instantiateViewController(withIdentifier: "kImplantDebridementConsentFormVC") as! ImplantDebridementConsentFormVC
            form.signPatient = signaturePatient.signatureImage()
            form.signWitness = signatureWitness.signatureImage()
            form.patient = self.patient
            self.navigationController?.pushViewController(form, animated: true)
            
        }
    }
    
}
