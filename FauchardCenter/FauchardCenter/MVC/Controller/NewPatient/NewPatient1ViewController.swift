//
//  NewPatient1ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/24/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient1ViewController: MCViewController {
    
    @IBOutlet weak var dropDownMaritalStatus : BRDropDown!
    @IBOutlet weak var dropDownGender : BRDropDown!
    @IBOutlet weak var radioLegalName : RadioButton!
    @IBOutlet weak var textfieldFormerName : MCTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()

    }

    func loadValue() {
                
        dropDownMaritalStatus.placeholder = "-- SELECT --"
        dropDownMaritalStatus.items = ["Single","Married","Divorced","Separated","Widowed"]
        
        dropDownGender.placeholder = "-- SELECT --"
        dropDownGender.items = ["Male","Female"]
        
        dropDownMaritalStatus.selectedIndex = patient.newPatient.maritalStatus
        dropDownGender.selectedIndex = patient.newPatient.gender
        radioLegalName.setSelectedWithTag(patient.newPatient.legalTag)
        textfieldFormerName.setSavedText(patient.newPatient.formerName)
        

    }
    
    func saveValue()  {
        patient.newPatient.maritalStatus = dropDownMaritalStatus.selectedOption == nil ? 0 : dropDownMaritalStatus.selectedIndex
        patient.newPatient.gender = dropDownGender.selectedOption == nil ? 0 : dropDownGender.selectedIndex
        patient.newPatient.legalTag = radioLegalName.selected == nil ? 0 : radioLegalName.selected.tag
        patient.newPatient.formerName = textfieldFormerName.getText()
       

    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        self.view.endEditing(true)
        if  dropDownMaritalStatus.selectedOption == nil || dropDownGender.selectedOption == nil || radioLegalName.selected == nil{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else{
            saveValue()
            let medical = newPatientStoryBoard.instantiateViewController(withIdentifier: "NewPatient2VC") as! NewPatient2ViewController
            medical.patient = self.patient
            self.navigationController?.pushViewController(medical, animated: true)

        }
    }

    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onLegalNameButtonPressed (withSender sender : RadioButton){
        if sender.tag == 2{
            PopupTextField.popUpView().showInViewController(self, WithTitle: "PLEASE ENTER YOUR LEGAL NAME", placeHolder: "NAME", textFormat: TextFormat.default, completion: { (popUpView, textField) in
                if textField.isEmpty{
                    self.patient.newPatient.legalName = "N/A"
                    sender.setSelectedWithTag(1)
                }else{
                    self.patient.newPatient.legalName = textField.text!
                }
                popUpView.removeFromSuperview()
            })
        }else{
            self.patient.newPatient.legalName = "N/A"

        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
