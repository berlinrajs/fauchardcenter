//
//  TestimonialFormViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class TestimonialFormViewController: MCViewController {

    var signPatient : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var imageviewPatientSign : UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelPatientName.text = patient.fullName
        labelDate.text = patient.dateToday
        imageviewPatientSign.image = signPatient
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
