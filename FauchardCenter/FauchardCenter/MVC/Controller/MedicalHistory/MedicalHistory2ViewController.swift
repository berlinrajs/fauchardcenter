//
//  MedicalHistory2ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/23/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistory2ViewController: MCViewController {
    
    @IBOutlet weak var textviewMedications : MCTextView!
    @IBOutlet weak var textfieldPhysicianName : MCTextField!
    @IBOutlet weak var textfieldPhysicianPhone : MCTextField!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelTitle : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        textviewMedications.placeholder = "PLEASE TYPE HERE"
        textfieldPhysicianPhone.textFormat = .phone
        labelDate.todayDate = patient.dateToday
        if patient.is18YearsOld{
            labelTitle.text = "Patient Signature"
            labelPatientName.text = patient.fullName
        }else{
            labelTitle.text = "Parent/Guardian Signature"
            labelPatientName.text = ""
        }
        textviewMedications.text = patient.medical.medications == "N/A" ? "PLEASE TYPE HERE" : patient.medical.medications
        textviewMedications.textColor = patient.medical.medications == "N/A" ? UIColor.lightGray : UIColor.black
        textfieldPhysicianName.text = patient.medical.physicianName == "N/A" ? "" : patient.medical.physicianName
        textfieldPhysicianPhone.text = patient.medical.physicianPhone == "N/A" ? "" : patient.medical.physicianPhone
    }
    
    func saveValue()  {
        patient.medical.medications = textviewMedications.isEmpty ? "N/A" : textviewMedications.text!
        patient.medical.physicianName = textfieldPhysicianName.isEmpty ? "N/A" : textfieldPhysicianName.text!
        patient.medical.physicianPhone = textfieldPhysicianPhone.isEmpty ? "N/A" : textfieldPhysicianPhone.text!
        
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldPhysicianPhone.isEmpty && !textfieldPhysicianPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID PHYSICIAN PHONE NUMBER")
        }else if !signaturePatient.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValue()
            patient.medical.patientSignature = signaturePatient.signatureImage()
            let medical = mainStoryBoard.instantiateViewController(withIdentifier: "MedicalFormVC") as! MedicalHistoryFormViewController
            medical.patient = self.patient
            self.navigationController?.pushViewController(medical, animated: true)

        }
    }
    
    
    
}
