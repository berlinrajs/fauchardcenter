//
//  NewPatient9ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/25/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient9ViewController: MCViewController {

    @IBOutlet weak var textfieldEmergencyName : MCTextField!
    @IBOutlet weak var textfieldEmergencyRelationship : MCTextField!
    @IBOutlet weak var textfieldEmergencyHomePhone : MCTextField!
    @IBOutlet weak var textfieldEmergencyWorkPhone : MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        textfieldEmergencyHomePhone.textFormat = .phone
        textfieldEmergencyWorkPhone.textFormat = .phone
        textfieldEmergencyName.setSavedText(patient.newPatient.emergencyName)
        textfieldEmergencyRelationship.setSavedText(patient.newPatient.emergencyRelationship)
        textfieldEmergencyHomePhone.setSavedText(patient.newPatient.emergencyHomePhone)
        textfieldEmergencyWorkPhone.setSavedText(patient.newPatient.emergencyWorkPhone)
        
    }
    
    func saveValue()  {
        patient.newPatient.emergencyName = textfieldEmergencyName.getText()
        patient.newPatient.emergencyRelationship = textfieldEmergencyRelationship.getText()
        patient.newPatient.emergencyHomePhone = textfieldEmergencyHomePhone.getText()
        patient.newPatient.emergencyWorkPhone = textfieldEmergencyWorkPhone.getText()
        
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        self.view.endEditing(true)
        if !textfieldEmergencyHomePhone.isEmpty && !textfieldEmergencyHomePhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID HOME PHONE NUMBER")
        }else if !textfieldEmergencyWorkPhone.isEmpty && !textfieldEmergencyWorkPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID WORK PHONE NUMBER")
        }else{
            saveValue()
            let new = newPatientStoryBoard.instantiateViewController(withIdentifier: "NewPatient10VC") as! NewPatient10ViewController
            new.patient = self.patient
            self.navigationController?.pushViewController(new, animated: true)
            
        }
    }
    
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }

    


}
