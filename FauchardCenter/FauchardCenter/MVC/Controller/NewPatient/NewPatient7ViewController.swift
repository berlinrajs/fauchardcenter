//
//  NewPatient7ViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/25/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient7ViewController: MCViewController {
    
    @IBOutlet weak var textfieldInsuranceName : MCTextField!
    @IBOutlet weak var textfieldSubscriberName : MCTextField!
    @IBOutlet weak var textfieldDOB : MCTextField!
    @IBOutlet weak var textfieldSSN : MCTextField!
    @IBOutlet weak var textfieldGroup : MCTextField!
    @IBOutlet weak var textfieldPhoneNumber : MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValue()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        textfieldDOB.textFormat = .dateIn1980
        textfieldSSN.textFormat = .socialSecurity
        textfieldPhoneNumber.textFormat = .phone
        textfieldGroup.textFormat = .alphaNumeric
        textfieldInsuranceName.setSavedText(patient.newPatient.primaryInsurance.insuranceName)
        textfieldSubscriberName.setSavedText(patient.newPatient.primaryInsurance.subscriberName)
        textfieldDOB.setSavedText(patient.newPatient.primaryInsurance.DateOfBirth)
        textfieldSSN.setSavedText(patient.newPatient.primaryInsurance.socialSecurity)
        textfieldGroup.setSavedText(patient.newPatient.primaryInsurance.group)
        textfieldPhoneNumber.setSavedText(patient.newPatient.primaryInsurance.phoneNumber)
    }
    
    func saveValue()  {
        patient.newPatient.primaryInsurance.insuranceName = textfieldInsuranceName.getText()
        patient.newPatient.primaryInsurance.subscriberName = textfieldSubscriberName.getText()
        patient.newPatient.primaryInsurance.DateOfBirth = textfieldDOB.getText()
        patient.newPatient.primaryInsurance.socialSecurity = textfieldSSN.getText()
        patient.newPatient.primaryInsurance.group = textfieldGroup.getText()
        patient.newPatient.primaryInsurance.phoneNumber = textfieldPhoneNumber.getText()
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        self.view.endEditing(true)
        if !textfieldSSN.isEmpty && textfieldSSN.text!.characters.count != 9 {
            self.showAlert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
        }else if !textfieldPhoneNumber.isEmpty && !textfieldPhoneNumber.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID INSURANCE PHONE NUMBER")
        }else{
            saveValue()
            YesOrNoAlert.sharedInstance.showWithTitle("DO YOU HAVE A SECONDARY INSURANCE", button1Title: "YES", button2Title: "NO", completion: { (buttonIndex) in
                if buttonIndex == 1{
                    let new = newPatientStoryBoard.instantiateViewController(withIdentifier: "NewPatient8VC") as! NewPatient8ViewController
                    new.patient = self.patient
                    self.navigationController?.pushViewController(new, animated: true)
                }else{
                    let new = newPatientStoryBoard.instantiateViewController(withIdentifier: "NewPatient9VC") as! NewPatient9ViewController
                    new.patient = self.patient
                    self.navigationController?.pushViewController(new, animated: true)
                }
            })
        }
    }
    
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    
    
    
    
}
