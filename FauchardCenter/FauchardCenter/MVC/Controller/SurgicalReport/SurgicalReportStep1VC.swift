//
//  SurgicalReportStep1VC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 29/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalReportStep1VC: SurgicalReportVC {

    @IBOutlet weak var textFieldRe: MCTextField!
    @IBOutlet weak var textFieldDateSurgery: MCTextField!
    @IBOutlet weak var textFieldSurgeryType: MCTextField!
    @IBOutlet weak var textFieldFlapSurgery: MCTextField!
    @IBOutlet weak var textFieldBoneGraftSurgery: MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if surgicalReport == nil {
            surgicalReport = SurgicalReport()
        }
        
        textFieldDateSurgery.textFormat = .dateInCurrentYear
        
        loadValues()
    }
    
    func loadValues() {
        textFieldRe.text = surgicalReport.reText
        textFieldDateSurgery.text = surgicalReport.dateOfSurgery
        textFieldSurgeryType.text = surgicalReport.typeOfSurgery
        textFieldFlapSurgery.text = surgicalReport.flapSurgery
        textFieldBoneGraftSurgery.text = surgicalReport.boneGraftSurgery
    }
    
    func saveValues() {
        self.view.endEditing(true)
        surgicalReport.reText = textFieldRe.text!
        surgicalReport.dateOfSurgery = textFieldDateSurgery.text!
        surgicalReport.typeOfSurgery = textFieldSurgeryType.text!
        surgicalReport.flapSurgery = textFieldFlapSurgery.text!
        surgicalReport.boneGraftSurgery = textFieldBoneGraftSurgery.text!
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonNextAction() {
        saveValues()
        
        let step2VC = self.storyboard?.instantiateViewController(withIdentifier: "kSurgicalReportStep2VC") as! SurgicalReportStep2VC
        step2VC.patient = self.patient
        self.navigationController?.pushViewController(step2VC, animated: true)
    }
}
