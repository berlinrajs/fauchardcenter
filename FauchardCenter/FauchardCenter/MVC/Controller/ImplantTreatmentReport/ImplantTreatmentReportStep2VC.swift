//
//  ImplantTreatmentReportStep2VC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 30/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ImplantTreatmentReportStep2VC: MCViewController {
    
    @IBOutlet weak var textFieldFinalRestoration: MCTextField!
    @IBOutlet weak var tableViewTreatmentNotes: UITableView!
    
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var labelName: UILabel!
    
    var treatmentReport: ImplantTreatmentReport! {
        get {
            return patient.treatmentReport
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if treatmentReport.treatmentNotes == nil {
            treatmentReport.treatmentNotes = [TreatmentNotes]()
        }

        tableViewTreatmentNotes.delegate = self
        tableViewTreatmentNotes.dataSource = self
        tableViewTreatmentNotes.reloadData()
        
        loadValues()
    }
    
    func loadValues() {
        labelDate.todayDate = patient.dateToday
        labelName.text = patient.dentistName
        textFieldFinalRestoration.text = treatmentReport.finalRestoration
    }
    
    func saveValues() {
        treatmentReport.signature = signatureView.signatureImage()
        treatmentReport.finalRestoration = textFieldFinalRestoration.text!
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonNextAction() {
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE TAP TO DATE")
        } else {
            saveValues()
            
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kImplantTreatmentReportFormVC") as! ImplantTreatmentReportFormVC
            formVC.patient = self.patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}

extension ImplantTreatmentReportStep2VC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == treatmentReport.treatmentNotes.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddCell")
            cell?.backgroundColor = UIColor.clear
            cell?.contentView.backgroundColor = UIColor.clear
            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditCell") as? TreatmentNotesTableViewCell
            cell?.configWithNotes(treatmentReport.treatmentNotes[indexPath.row])
            
            return cell!
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == treatmentReport.treatmentNotes.count {
            TreatmentNotePopUp.popUpView().showInViewController(self, withTreatmentNote: nil, deletionBlock: nil, completion: { (popUpView, location, type, length, healing, boneType, placement, lotNumber) in
                
                popUpView.close()
                
                let note = TreatmentNotes()
                note.location = location
                note.type = type
                note.length = length
                note.healing = healing
                note.boneType = boneType
                note.placement = placement
                note.lotNumber = lotNumber
                
                self.treatmentReport.treatmentNotes.append(note)
                self.tableViewTreatmentNotes.reloadData()
            })
        } else {
            TreatmentNotePopUp.popUpView().showInViewController(self, withTreatmentNote: treatmentReport.treatmentNotes[indexPath.row], deletionBlock: { (popUpView) in
                popUpView.close()
                self.treatmentReport.treatmentNotes.remove(at: indexPath.row)
                self.tableViewTreatmentNotes.reloadData()
                }, completion: { (popUpView, location, type, length, healing, boneType, placement, lotNumber) in
                    popUpView.close()
            })
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if treatmentReport.treatmentNotes.count < 4 {
            return treatmentReport.treatmentNotes.count + 1
        } else {
            return 4
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return treatmentReport.treatmentNotes == nil ? 0 : 1
    }
    
    
}
