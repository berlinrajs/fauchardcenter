//
//  SurgicalRecordStep1VC.swift
//  FauchardCenter
//
//  Created by Berlin Raj on 25/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalRecordStep1VC: SurgicalRecordVC {

    @IBOutlet weak var dropDownTreatment: BRDropDown!
    @IBOutlet var buttonSurgicalSiteOptions: [UIButton]!
    @IBOutlet weak var radioAnterior: RadioButton!
    @IBOutlet weak var textViewAnatomicalLandmark: MCTextView!
    
    var otherTreatment: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dropDownTreatment.items = ["Bone Grafting", "Extractions", "Implant Surgery", "Periodontal Disease Treatment", "Connective Tissues", "Biopsy/Excision", "Other"]
        dropDownTreatment.placeholder = "-- TREATMENT * --"
        dropDownTreatment.delegate = self
        
        loadValues()
    }
    
    func loadValues() {
        if self.surgicalRecord == nil {
            self.surgicalRecord = SurgicalRecord()
        }
        
        if surgicalRecord.surgicalSiteTags == nil {
            surgicalRecord.surgicalSiteTags = [Int]()
        }
        
        dropDownTreatment.selectedIndex = self.surgicalRecord.treatmentTag == nil ? 0 : self.surgicalRecord.treatmentTag
        self.otherTreatment = self.surgicalRecord.otherTreatment
        
        radioAnterior.setSelectedWithTag(self.surgicalRecord.isAnterior == nil ? 0 : self.surgicalRecord.isAnterior)
        
        textViewAnatomicalLandmark.textValue = self.surgicalRecord.anatomicalLandmark
        
        for button in buttonSurgicalSiteOptions {
            button.isSelected = surgicalRecord.surgicalSiteTags.contains(button.tag)
        }
    }
    
    func saveValues() {
        self.view.endEditing(true)
        
        self.surgicalRecord.treatmentTag = dropDownTreatment.selectedIndex
        self.surgicalRecord.otherTreatment = self.otherTreatment
        
        self.surgicalRecord.isAnterior = radioAnterior.selected == nil ? 0 : radioAnterior.selected.tag
        self.surgicalRecord.anatomicalLandmark = textViewAnatomicalLandmark.textValue
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonNextAction() {
        if dropDownTreatment.selectedIndex == 0 {
            self.showAlert("PLEASE SELECT TREATMENT")
        } else {
            saveValues()
            
            let step2VC = self.storyboard?.instantiateViewController(withIdentifier: "kSurgicalRecordStep2VC") as! SurgicalRecordStep2VC
            step2VC.patient = self.patient
            self.navigationController?.pushViewController(step2VC, animated: true)
        }
    }

    @IBAction func buttonSurgicalSiteAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            if !surgicalRecord.surgicalSiteTags.contains(sender.tag) {
                surgicalRecord.surgicalSiteTags.append(sender.tag)
            }
        } else {
            if surgicalRecord.surgicalSiteTags.contains(sender.tag) {
                surgicalRecord.surgicalSiteTags.remove(at: surgicalRecord.surgicalSiteTags.index(of: sender.tag)!)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension SurgicalRecordStep1VC: BRDropDownDelegate {
    func dropDown(_ dropDown: BRDropDown, selectedAtIndex index: Int, selectedOption option: String?) {
        if index == 7 {
            PopupTextField.popUpView().showInViewController(self, WithTitle: "TREATMENT", placeHolder: "TYPE HERE", textFormat: .default, completion: { (popUpView, textField) in
                popUpView.close()
                if textField.isEmpty {
                    self.otherTreatment = ""
                    dropDown.reset()
                } else {
                    self.otherTreatment = textField.text!
                }
            })
        } else {
            self.otherTreatment = ""
        }
    }
}
