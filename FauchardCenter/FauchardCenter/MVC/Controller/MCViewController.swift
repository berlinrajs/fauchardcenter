//
//  MCViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


let mainStoryBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
let consentStoryBoard = UIStoryboard(name: "Consent", bundle: Bundle.main)
let newPatientStoryBoard = UIStoryboard(name: "NewPatient", bundle: Bundle.main)
let surgicalStoryBoard = UIStoryboard(name: "SurgicalRecord", bundle: Bundle.main)


class MCViewController: UIViewController {
    
    @IBOutlet var buttonSubmit: MCButton?
    @IBOutlet var buttonBack: MCButton?
    
    @IBOutlet var pdfView: UIScrollView?
    
    var patient: MCPatient!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = UIRectEdge()
        
        self.buttonSubmit?.backgroundColor = UIColor.green
        self.buttonBack?.isHidden = self.buttonSubmit == nil && isFromPreviousForm
        
        if self.buttonBack != nil && self.buttonSubmit == nil && isFromPreviousForm {
            self.navigationController?.viewControllers.removeSubrange(1...self.navigationController!.viewControllers.index(of: self)! - 1)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var isFromPreviousForm: Bool {
        get {
            return navigationController?.viewControllers.count > 3 ? true : false
        }
    }
    
    @IBAction func buttonBackAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSubmitButtonPressed() {
        if !Reachability.isConnectedToNetwork() {
            self.showAlert("Your device is not connected to internet. Please go to settings to connect.", buttonTitles: ["Settings", "Cancel"], completion: { (buttonIndex) in
                if buttonIndex == 0 {
                    let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url)
                    }
                } else {
                    
                }
            })
            return
        }
        let pdfManager = PDFManager.sharedInstance()
        pdfManager.authorizeDrive(self) { (success) -> Void in
            if success {
                self.buttonSubmit?.isHidden = true
                self.buttonBack?.isHidden = true
                
                pdfManager.uploadToGoogleDrive(self.pdfView == nil ? self.view : self.pdfView!, patient: self.patient, completionBlock: { (finished) -> Void in
                    if finished {
                        self.patient.selectedForms.removeFirst()
                        self.gotoNextForm()
                    } else {
                        self.buttonSubmit?.isHidden = false
                        self.buttonBack?.isHidden = false
                    }
                })
            } else {
                self.buttonSubmit?.isHidden = false
                self.buttonBack?.isHidden = false
            }
        }
    }
    
    
    func gotoNextForm() {
        let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
        
        if formNames.contains(kNewPatientSignInForm){
            let medical = newPatientStoryBoard.instantiateViewController(withIdentifier: "NewPatient1VC") as! NewPatient1ViewController
            medical.patient = self.patient
            self.navigationController?.pushViewController(medical, animated: true)
        } else if formNames.contains(kMedicalHistory){
            let medical = mainStoryBoard.instantiateViewController(withIdentifier: "Medical1VC") as! MedicalHistory1ViewController
            medical.patient = self.patient
            self.navigationController?.pushViewController(medical, animated: true)
        } else if formNames.contains(kSurgicalForm) {
            let surgical = surgicalStoryBoard.instantiateViewController(withIdentifier: "kSurgicalRecordStep1VC") as! SurgicalRecordStep1VC
            surgical.patient = self.patient
            self.navigationController?.pushViewController(surgical, animated: true)
        } else if formNames.contains(kSurgicalWTForm) {
            let surgical = surgicalStoryBoard.instantiateViewController(withIdentifier: "kSurgicalRecordWisdomStep1VC") as! SurgicalRecordWisdomStep1VC
            surgical.patient = self.patient
            self.navigationController?.pushViewController(surgical, animated: true)
        } else if formNames.contains(kSurgicalReport) {
            let surgical = surgicalStoryBoard.instantiateViewController(withIdentifier: "kSurgicalReportStep1VC") as! SurgicalReportStep1VC
            surgical.patient = self.patient
            self.navigationController?.pushViewController(surgical, animated: true)
        } else if formNames.contains(kImplantTreatmentReport) {
            let surgical = surgicalStoryBoard.instantiateViewController(withIdentifier: "kImplantTreatmentReportStep1VC") as! ImplantTreatmentReportStep1VC
            surgical.patient = self.patient
            self.navigationController?.pushViewController(surgical, animated: true)
        } else if formNames.contains(kInsuranceCard) {
            let insuranceCard = mainStoryBoard.instantiateViewController(withIdentifier: "kCardImageCaptureVC") as! CardImageCaptureVC
            insuranceCard.patient = self.patient
            self.navigationController?.pushViewController(insuranceCard, animated: true)
        } else if formNames.contains(kDrivingLicense) {
            let drivingLicense = mainStoryBoard.instantiateViewController(withIdentifier: "kCardImageCaptureVC") as! CardImageCaptureVC
            drivingLicense.patient = self.patient
            drivingLicense.isDrivingLicense = true
            self.navigationController?.pushViewController(drivingLicense, animated: true)
        } else if formNames.contains(kCrownSurgery){
                let crown = consentStoryBoard.instantiateViewController(withIdentifier: "CrownSurgery1VC") as! CrownSurgery1ViewController
                crown.patient = self.patient
                self.navigationController?.pushViewController(crown, animated: true)
            } else if formNames.contains(kFrenulectomy){
                let frenulectomy = consentStoryBoard.instantiateViewController(withIdentifier: "Frenulectomy1VC") as! Frenulectomy1ViewController
                frenulectomy.patient = self.patient
                self.navigationController?.pushViewController(frenulectomy, animated: true)
            } else if formNames.contains(kImplantSurgery){
                let implant = consentStoryBoard.instantiateViewController(withIdentifier: "ImplantSurgery1VC") as! ImplantSurgery1ViewController
                implant.patient = self.patient
                self.navigationController?.pushViewController(implant, animated: true)
        } else if formNames.contains(kSedation){
            let sedation = consentStoryBoard.instantiateViewController(withIdentifier: "Sedation1VC") as! Sedation1ViewController
            sedation.patient = self.patient
            self.navigationController?.pushViewController(sedation, animated: true)
        } else if formNames.contains(kOpenSinus){
            let openSinus = consentStoryBoard.instantiateViewController(withIdentifier: "ElevationSurgery1VC") as! ElevationSurgery1ViewController
            openSinus.patient = self.patient
            self.navigationController?.pushViewController(openSinus, animated: true)
        } else if formNames.contains(kPeriodontal){
            let periodontal = consentStoryBoard.instantiateViewController(withIdentifier: "Periodontal1VC") as! Periodontal1ViewController
            periodontal.patient = self.patient
            self.navigationController?.pushViewController(periodontal, animated: true)
        } else if formNames.contains(kClosedSinus){
            let closedSinus = consentStoryBoard.instantiateViewController(withIdentifier: "ClosedSinus1VC") as! ClosedSinus1ViewController
            closedSinus.patient = self.patient
            self.navigationController?.pushViewController(closedSinus, animated: true)
        } else if formNames.contains(kRidgeAugmented){
            let ridge = consentStoryBoard.instantiateViewController(withIdentifier: "kRidge1ViewController") as! Ridge1ViewController
            ridge.patient = self.patient
            self.navigationController?.pushViewController(ridge, animated: true)
        } else if formNames.contains(kTissueGrafting){
            let tissue = consentStoryBoard.instantiateViewController(withIdentifier: "Tissue1VC") as! TissueGrafting1ViewController
            tissue.patient = self.patient
            self.navigationController?.pushViewController(tissue, animated: true)
        } else if formNames.contains(kRootPlaning){
            let root = consentStoryBoard.instantiateViewController(withIdentifier: "kRootPlaning1ViewController") as! RootPlaning1ViewController
            root.patient = self.patient
            self.navigationController?.pushViewController(root, animated: true)
        } else if formNames.contains(kTestimonial){
            let test = consentStoryBoard.instantiateViewController(withIdentifier: "Testimonial1VC") as! Testimonial1ViewController
            test.patient = self.patient
            self.navigationController?.pushViewController(test, animated: true)
        } else if formNames.contains(kToothRemoval){
            let tooth = consentStoryBoard.instantiateViewController(withIdentifier: "ToothRemoval1VC") as! ToothRemoval1ViewController
            tooth.patient = self.patient
            self.navigationController?.pushViewController(tooth, animated: true)
        } else if formNames.contains(kTreatment){
            let treat = consentStoryBoard.instantiateViewController(withIdentifier: "Treatment1VC") as! Treatment1ViewController
            treat.patient = self.patient
            self.navigationController?.pushViewController(treat, animated: true)
        } else if formNames.contains(kPAOO){
            let form = consentStoryBoard.instantiateViewController(withIdentifier: "kPaooVC") as! PaooVC
            form.patient = self.patient
            self.navigationController?.pushViewController(form, animated: true)
        } else if formNames.contains(kGingivialAugmentation){
            let form = consentStoryBoard.instantiateViewController(withIdentifier: "kGingivialAugmentationVC") as! GingivialAugmentationVC
            form.patient = self.patient
            self.navigationController?.pushViewController(form, animated: true)
        } else if formNames.contains(kSinusAugmentation){
            let form = consentStoryBoard.instantiateViewController(withIdentifier: "kSinusAugmentationVC") as! SinusAugmentationVC
            form.patient = self.patient
            self.navigationController?.pushViewController(form, animated: true)
        } else if formNames.contains(kImplantDebridmentConsent){
            let form = consentStoryBoard.instantiateViewController(withIdentifier: "kImplantDebridementConsentVC") as! ImplantDebridementConsentVC
            form.patient = self.patient
            self.navigationController?.pushViewController(form, animated: true)
        }else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: kFormsCompletedNotification), object: nil)
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}
