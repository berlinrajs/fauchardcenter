//
//  FrenulectomyFormViewController.swift
//  FauchardCenter
//
//  Created by Bala Murugan on 11/21/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class FrenulectomyFormViewController: MCViewController {

    var signPatient : UIImage!
    var signWitness : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var imageviewPatientSign : UIImageView!
    @IBOutlet weak var imageviewWitnessSign : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelWitnessName : UILabel!
    @IBOutlet weak var labelToothNumber : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelPatientName.text = patient.fullName
        labelDate.text = patient.dateToday
        imageviewPatientSign.image = signPatient
        imageviewWitnessSign.image = signWitness
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        let form : Forms = patient.selectedForms.first!
        labelToothNumber.text = form.toothNumbers
        labelWitnessName.text = patient.consentWitnessName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
